--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFControls = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				MainInput1 = InstanceInput {
					SourceOp = "Controls",
					Source = "Image1",
				},
				Light = InstanceInput {
					SourceOp = "Controls",
					Source = "PointIn1",
				},
				Axis = InstanceInput {
					SourceOp = "Controls",
					Source = "PointIn2",
				},
				MultiIris = InstanceInput {
					SourceOp = "Controls",
					Source = "MultiIris",
				},
				Occlusion = InstanceInput {
					SourceOp = "Controls",
					Source = "NumberIn2",
					Default = 0,
				},
				Aberrations = InstanceInput {
					SourceOp = "Controls",
					Source = "NumberIn3",
					Default = 0,
				},
				Hoop = InstanceInput {
					SourceOp = "Controls",
					Source = "Hoop",
				},
				HoopOffset = InstanceInput {
					SourceOp = "Controls",
					Source = "NumberIn6",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Controls",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 1112, 155.242 },
				Flags = {
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 126, 66.3636, 63, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0.666667, 0 }
			},
			Tools = ordered() {
				Controls = Custom {
					NameSet = true,
					Inputs = {
						PointIn1 = Input { Value = { 0.3, 0.7 }, },
						PointIn4 = Input {
							Value = { 0.7, 0.3 },
							Expression = "PointIn1*NumberIn1+PointIn2*(1-NumberIn1)",
						},
						NumberIn1 = Input { Value = -1, },
						NumberIn5 = Input { Value = 1, },
						LUTIn1 = Input {
							SourceOp = "ControlsLUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "ControlsLUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "ControlsLUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "ControlsLUTIn4",
							Source = "Value",
						},
						RedExpression = Input { Value = "0", },
						GreenExpression = Input { Value = "0", },
						BlueExpression = Input { Value = "0", },
						AlphaExpression = Input { Value = "1", },
						NumberControls = Input { Value = 1, },
						ShowNumber1 = Input { Value = 0, },
						NameforNumber1 = Input { Value = "MainOffset", },
						NameforNumber2 = Input { Value = "Occlusion", },
						NameforNumber3 = Input { Value = "Aberration", },
						NameforNumber4 = Input { Value = "Start", },
						NameforNumber5 = Input { Value = "End", },
						NameforNumber6 = Input { Value = "HoopOffset", },
						ShowNumber7 = Input { Value = 0, },
						ShowNumber8 = Input { Value = 0, },
						PointControls = Input { Value = 1, },
						NameforPoint1 = Input { Value = "Light", },
						NameforPoint2 = Input { Value = "Axis", },
						ShowPoint3 = Input { Value = 0, },
						NameforPoint3 = Input { Value = "Hold", },
						ShowPoint4 = Input { Value = 0, },
						NameforPoint4 = Input { Value = "End", },
						OcRig = Input { Expression = "PointIn1*NumberIn2+PointIn3*(1-NumberIn2)", },
						AbRig = Input { Expression = "PointIn1*NumberIn3+PointIn3*(1-NumberIn3)", },
						HoRig = Input { Expression = "PointIn1*NumberIn6+PointIn2*(1-NumberIn6)", },
						MultiIris = Input { Value = 1, },
						Hoop = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { -0.666667, 8.75758 } },
					UserControls = ordered() {
						OcRig = {
							LINKS_Name = "OcRig",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls"
						},
						AbRig = {
							LINKS_Name = "AbRig",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls"
						},
						HoRig = {
							LINKS_Name = "HoRig",
							LINKID_DataType = "Point",
							INPID_InputControl = "OffsetControl",
							ICS_ControlPage = "Controls"
						},
						MultiIris = {
							INP_Integer = false,
							LBLC_DropDownButton = true,
							LINKID_DataType = "Number",
							LBLC_NumInputs = 4,
							INPID_InputControl = "LabelControl",
							LINKS_Name = "Multi Iris",
						},
						Hoop = {
							INP_Integer = false,
							LBLC_DropDownButton = true,
							LINKID_DataType = "Number",
							LBLC_NumInputs = 1,
							INPID_InputControl = "LabelControl",
							LINKS_Name = "Hoop",
						}
					}
				},
				ControlsLUTIn1 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 0, Blue = 0 },
					NameSet = true,
				},
				ControlsLUTIn2 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 204, Blue = 0 },
					NameSet = true,
				},
				ControlsLUTIn3 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 0, Blue = 204 },
					NameSet = true,
				},
				ControlsLUTIn4 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 204, Blue = 204 },
					NameSet = true,
				}
			},
		}
	},
	ActiveTool = "ml_LFControls"
}