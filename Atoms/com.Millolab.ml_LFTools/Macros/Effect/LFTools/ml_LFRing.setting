--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFRing = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "RING",
					Source = "Background",
				},
				Controls = InstanceInput {
					SourceOp = "RING",
					Source = "Controls",
					Default = 0,
				},
				Blank1 = InstanceInput {
					SourceOp = "RING",
					Source = "Blank1",
				},
				Blend = InstanceInput {
					SourceOp = "RING",
					Source = "BlendClone",
					Name = "Main Blend",
					Default = 0.2,
				},
				Size = InstanceInput {
					SourceOp = "RING",
					Source = "SizeCopy",
					Name = "Size",
					Default = 3,
				},
				Angle = InstanceInput {
					SourceOp = "RING",
					Source = "Angle",
					Default = 128,
				},
				Blank2 = InstanceInput {
					SourceOp = "RING",
					Source = "Blank1",
				},
				Lenght = InstanceInput {
					SourceOp = "Rectangle",
					Source = "Size",
					Name = "Lenght",
					Page = "Controls",
					Default = 0.1,
				},
				SoftEdge = InstanceInput {
					SourceOp = "Blur",
					Source = "XBlurSize",
					Name = "SoftEdge",
					Default = 40,
				},
				Thickness = InstanceInput {
					SourceOp = "BG",
					Source = "Thickness",
					MinScale = 0.1,
					MaxScale = 7,
					Default = 1.8,
				},
				LockXY = InstanceInput {
					SourceOp = "Blur1",
					Source = "LockXY",
					Default = 1,
				},
				XBlurSize = InstanceInput {
					SourceOp = "Blur1",
					Source = "XBlurSize",
					Name = "Ring Blur",
					Default = 20,
				},
				YBlurSize = InstanceInput {
					SourceOp = "Blur1",
					Source = "YBlurSize",
					Default = 4,
				},
				Blank3 = InstanceInput {
					SourceOp = "RING",
					Source = "Blank1",
				},
				Gradient = InstanceInput {
					SourceOp = "BG",
					Source = "Gradient",
				},
				Blank4 = InstanceInput {
					SourceOp = "RING",
					Source = "Blank1",
				},
				Spectrum = InstanceInput {
					SourceOp = "BG",
					Source = "Offset",
					Name = "Spectrum",
					Default = 0,
				},
				Repeat = InstanceInput {
					SourceOp = "BG",
					Source = "Repeat",
				},
				InvertSpectrum = InstanceInput {
					SourceOp = "BG",
					Source = "Invert",
					Name = "Invert Spectrum",
					Default = 0,
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "RING",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 466.667, 291 },
				Flags = {
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 1110.41, 292.971, 610.871, 103.03 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 2.95492, -2.88293 }
			},
			Tools = ordered() {
				RING = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.2, },
						Foreground = Input {
							SourceOp = "Blur1",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.3, 0.7 },
							Expression = "self:GetSourceTool(\"Controls\").Light",
						},
						Size = Input {
							Value = 3,
							Expression = "SizeCopy",
						},
						Angle = Input { Value = 128, },
						Gain = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 275.873, 87.5576 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							INP_Default = 0,
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							LINKS_Name = "Controls",
						},
						SizeCopy = {
							INP_Default = 3,
							INP_Integer = false,
							ICD_Center = 1,
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 5,
							LINKS_Name = "SizeCopy",
						}
					}
				},
				Rectangle = RectangleMask {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Width = Input {
							Value = 0.1,
							Expression = "Size",
						},
						Height = Input { Value = 1, },
						Size = Input { Value = 0.1, },
					},
					ViewInfo = OperatorInfo { Pos = { -481.622, 36.4897 } },
					UserControls = ordered() {
						Size = {
							INP_Default = 0.5,
							INP_Integer = false,
							ICS_ControlPage = "Controls",
							LINKID_DataType = "Number",
							INP_MinScale = 0,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 1,
							LINKS_Name = "Size"
						}
					}
				},
				Blur = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						LockXY = Input { Value = 0, },
						XBlurSize = Input { Value = 40, },
						Input = Input {
							SourceOp = "BG",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -323.348, 87.5576 } },
				},
				BG = Background {
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Rectangle",
							Source = "Mask",
						},
						Width = Input {
							Value = 240,
							Expression = "RING.Background.OriginalWidth/8",
						},
						Height = Input {
							Value = 35,
							Expression = "RING.Background.OriginalWidth/100*Thickness",
						},
						Depth = Input { Value = 4, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Type = Input { Value = FuID { "Gradient" }, },
						TopLeftRed = Input { Value = 1, },
						TopLeftGreen = Input { Value = 1, },
						TopLeftBlue = Input { Value = 1, },
						Start = Input {
							Value = { 0.5, 0 },
							Expression = "Point(0.5, 1.0*(1-Invert))",
						},
						End = Input {
							Value = { 0.5, 1 },
							Expression = "Point(0.5, 1.0*Invert)",
						},
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 0, 0, 1, 1 },
									[0.166666666] = { 1, 0, 1, 1 },
									[0.333333333] = { 1, 0, 0, 1 },
									[0.5] = { 0.9999994635582, 1, 2.644956e-07, 1 },
									[0.6666666666] = { 0, 1, 4.377216e-07, 1 },
									[0.83333333333] = { 0, 1, 1, 1 },
									[1] = { 0, 0, 1, 1 }
								}
							},
						},
						GradientInterpolationMethod = Input { Value = FuID { "HLS" }, },
						Repeat = Input { Value = FuID { "Repeat" }, },
						Invert = Input { Value = 1, },
						Thickness = Input { Value = 1.8, }
					},
					ViewInfo = OperatorInfo { Pos = { -481.622, 87.5576 } },
					UserControls = ordered() {
						Invert = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Invert",
						},
						Thickness = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 10,
							INP_Default = 1,
							INP_MinScale = 1,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							LINKS_Name = "Thickness",
						}
					}
				},
				Merge = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Background = Input {
							SourceOp = "BGRef",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Blur",
							Source = "Output",
						},
						Center = Input { Value = { 0.5, 0.360917941585535 }, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -185.676, 87.5576 } },
				},
				CS = CoordSpace {
					NameSet = true,
					Inputs = {
						Shape = Input { Value = 1, },
						Input = Input {
							SourceOp = "Merge",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -34.5334, 87.5576 } },
				},
				BGRef = Background {
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 240,
							Expression = "RING.Background.OriginalWidth/8",
						},
						Height = Input { Expression = "Width", },
						Depth = Input { Value = 4, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						TopLeftAlpha = Input { Value = 0, },
						Start = Input { Value = { 0.5, 1 }, },
						End = Input { Value = { 0.5, 0 }, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 1, 0, 0, 1 },
									[0.16666666] = { 0.9999994635582, 1, 2.644956e-07, 1 },
									[0.333333333333] = { 0, 1, 4.377216e-07, 1 },
									[0.5] = { 0, 1, 1, 1 },
									[0.66666666] = { 0, 0, 1, 1 },
									[0.866666666] = { 1, 0, 1, 1 },
									[1] = { 1, 0, 0, 1 }
								}
							},
						},
						Repeat = Input { Value = FuID { "Repeat" }, },
					},
					ViewInfo = OperatorInfo { Pos = { -185.676, 142.276 } },
				},
				Blur1 = Blur {
					NameSet = true,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						XBlurSize = Input { Value = 20, },
						YBlurSize = Input { Value = 4, },
						Input = Input {
							SourceOp = "CS",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 115.425, 87.5576 } },
				}
			},
		}
	},
	ActiveTool = "ml_LFRing"
}