--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFIris = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "Merge4",
					Source = "Background",
				},
				Controls = InstanceInput {
					SourceOp = "IRIS",
					Source = "Controls",
					Default = 0,
				},
				Blank9 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				Start = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "WriteOnStart",
					ControlGroup = 15,
					Default = 0,
				},
				End = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "WriteOnEnd",
					ControlGroup = 15,
					Default = 1,
				},
				ClonesCount = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "ClonesCount",
					Name = "Iris Copies",
					Default = 25,
				},
				Blank1 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				ElementStrength = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "ElementStrength1",
					Name = "Element Strength",
					Default = 0.05,
				},
				ElementSize = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "ElementSize1",
					Name = "Element Size",
					Default = 0.8,
				},
				ElementType = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "ElementType1",
					Name = "Element Type",
					Default = 2,
				},
				NGonAngle = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "NGonAngle1",
					Name = "NGon Angle",
					Default = 0,
				},
				NGonSides = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "NGonSides1",
					Name = "NGon Sides",
					Default = 5,
				},
				NGonStarryness = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "NGonStarryness1",
					Name = "NGon Starryness",
					Default = 0,
				},
				Blank3 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				LensRed = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "LensRed1",
					Name = "Lens",
					ControlGroup = 9,
					Default = 1,
				},
				LensGreen = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "LensGreen1",
					ControlGroup = 9,
					Default = 1,
				},
				LensBlue = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "LensBlue1",
					ControlGroup = 9,
					Default = 1,
				},
				LensAlpha = InstanceInput {
					SourceOp = "HotSpot1",
					Source = "LensAlpha1",
					ControlGroup = 9,
					Default = 1,
				},
				Blank4 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				LockXY = InstanceInput {
					SourceOp = "Scale1",
					Source = "LockXY",
					Default = 1,
				},
				SizeMultiplier = InstanceInput {
					SourceOp = "Scale1",
					Source = "XSize",
					Name = "Size Multiplier",
					Default = 1,
				},
				YSizeMultiplier = InstanceInput {
					SourceOp = "Scale1",
					Source = "YSize",
					Default = 1,
				},
				LinearOffset = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "LinearOffset",
					Default = 0,
				},
				XSize = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "XSize",
					Name = "Size Variation",
					MaxScale = 1.1,
					Default = 1.05,
				},
				Opacity = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "LayerBlend",
					Name = "Opacity Variation",
					Default = 1,
				},
				Blank5 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				Aberrations = InstanceInput {
					SourceOp = "IRIS",
					Source = "EnableAberrations",
					Page = "Controls",
					Default = 0,
				},
				Diffraction = InstanceInput {
					SourceOp = "DirectionalBlur1",
					Source = "Diffraction",
					Page = "Controls",
					Default = 0,
				},
				AberrationsBlend = InstanceInput {
					SourceOp = "BrightnessContrast1",
					Source = "Blend",
					Name = "Occlusion Amount",
					Default = 1,
				},
				SoftEdge = InstanceInput {
					SourceOp = "Bitmap1",
					Source = "SoftEdge",
					Name = "Occlusion Soft",
				},
				Blank2 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank1",
				},
				JitterAxis = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAxis",
					Name = "Jitter Position",
					DefaultX = 0,
					DefaultY = 0,
				},
				JitterXSize = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterXSize",
					Name = "Jitter Size",
					Default = 0.8,
				},
				JitterAngle = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAngle",
					Name = "Jitter Angle",
					Default = 0,
				},
				JitterRedGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterRedGain",
					Name = "Jitter Red",
					Default = 0,
				},
				JitterGreenGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterGreenGain",
					Name = "Jitter Green",
					Default = 0,
				},
				JitterBlueGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterBlueGain",
					Name = "Jitter Blue",
					Default = 0,
				},
				JitterAlphaGain = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterAlphaGain",
					Name = "Jitter Alpha",
					Default = 0,
				},
				JitterLayerBlend = InstanceInput {
					SourceOp = "LinearCloner1",
					Source = "JitterLayerBlend",
					Name = "Jitter Opacity",
					Default = 0,
				},
				Blank6 = InstanceInput {
					SourceOp = "Merge4",
					Source = "Blank2",
				},
				Randomize = InstanceInput {
					SourceOp = "Merge4",
					Source = "Randomize",
					Page = "Controls",
					Default = 26024,
				},
				InvertMask = InstanceInput {
					SourceOp = "MASK_In",
					Source = "ApplyMaskInverted",
					Page = "Common",
				},
				MaskChannel = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskChannel",
					Default = 3,
				},
				MaskLow = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskLow",
					ControlGroup = 11,
					Default = 0,
				},
				MaskHigh = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskHigh",
					ControlGroup = 11,
					Default = 1,
				},
				Black = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				White = InstanceInput {
					SourceOp = "MASK_In",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				EffectMask = InstanceInput {
					SourceOp = "MASK_In",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "Merge4",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 374, -135.667 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 629.169, 423.257, 295.023, 44.4448 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -5.68873, -14.6778 }
			},
			Tools = ordered() {
				IRIS = Background {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 240,
							Expression = "Merge4.Background.OriginalWidth/8",
						},
						Height = Input {
							Value = 135,
							Expression = "Merge4.Background.OriginalHeight/8",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						BackgroundNest = Input { Value = 0, },
						TopLeftAlpha = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -214.98, 9.01199 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							LINKS_Name = "Controls",
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							INP_Default = 0,
						},
						EnableAberrations = {
							CBC_TriState = false,
							INP_Integer = false,
							LINKID_DataType = "Number",
							INPID_InputControl = "CheckboxControl",
							LINKS_Name = "Enable Aberrations",
						}
					}
				},
				HotSpot1 = HotSpot {
					CtrlWShown = false,
					Inputs = {
						PrimaryStrength = Input { Value = 0, },
						AmbientLight = Input { Disabled = true, },
						Red = Input {
							SourceOp = "HotSpot1Red",
							Source = "Value",
						},
						Green = Input {
							SourceOp = "HotSpot1Green",
							Source = "Value",
						},
						Blue = Input {
							SourceOp = "HotSpot1Blue",
							Source = "Value",
						},
						Alpha = Input {
							SourceOp = "HotSpot1Alpha",
							Source = "Value",
						},
						Mix = Input {
							SourceOp = "HotSpot1Mix",
							Source = "Value",
						},
						Radial = Input {
							SourceOp = "HotSpot1Radial",
							Source = "Value",
						},
						Length = Input {
							SourceOp = "HotSpot1Length",
							Source = "Value",
						},
						Element1Reflection1 = Input { Value = 1, },
						ElementStrength1 = Input { Value = 0.05, },
						ElementSize1 = Input { Value = 0.8, },
						ElementType1 = Input { Value = 2, },
						LensGreen1 = Input { Value = 1, },
						LensBlue1 = Input { Value = 1, },
						Input = Input {
							SourceOp = "IRIS",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -214.98, 58.9205 } },
				},
				HotSpot1Red = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 255, Green = 52, Blue = 52 },
				},
				HotSpot1Green = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 255, Blue = 0 },
				},
				HotSpot1Blue = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 68, Green = 68, Blue = 255 },
				},
				HotSpot1Alpha = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 255, Green = 255, Blue = 255 },
				},
				HotSpot1Mix = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 255, Blue = 255 },
				},
				HotSpot1Radial = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 255, Green = 170, Blue = 48 },
				},
				HotSpot1Length = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 170, Green = 128, Blue = 255 },
				},
				Transform3 = Transform {
					CtrlWZoom = false,
					CtrlWShown = false,
					Inputs = {
						Blend = Input { Expression = "IRIS.EnableAberrations", },
						ProcessRed = Input { Value = 0, },
						ProcessBlue = Input { Value = 0, },
						Center = Input {
							Value = { 0.513452908693346, 0.512820356772324 },
							Expression = "IRIS:GetSourceTool(\"Controls\").AbRig",
						},
						Input = Input {
							SourceOp = "HotSpot1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -214.98, 108.841 } },
				},
				Bitmap1 = BitmapMask {
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						MaskWidth = Input { Value = 1920, },
						MaskHeight = Input { Value = 1080, },
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Image = Input {
							SourceOp = "Instance_Transform3",
							Source = "Output",
						},
						Center = Input { Expression = "IRIS:GetSourceTool(\"Controls\").OcRig", },
						Channel = Input { Value = FuID { "Luminance" }, },
						High = Input { Value = 0.03, },
					},
					ViewInfo = OperatorInfo { Pos = { -38.715, 109.149 } },
				},
				Instance_Transform3 = Transform {
					CtrlWShown = false,
					SourceOp = "Transform3",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						Blend = Input { Expression = "IRIS.EnableAberrations" },
						ProcessRed = Input { Value = 0, },
						ProcessGreen = Input { Value = 0, },
						ProcessBlue = Input { },
						TransformNest = Input { },
						Center = Input {
							Value = { 0.483516753623188, 0.512259543300302 },
							Expression = "IRIS:GetSourceTool(\"Controls\").AbRig"
						},
						InvertTransform = Input { Value = 1, },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "Transform3",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { -214.98, 170.284 } },
				},
				BrightnessContrast1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						EffectMask = Input {
							SourceOp = "Bitmap1",
							Source = "Mask",
						},
						ApplyMaskInverted = Input { Value = 1, },
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0, },
						Input = Input {
							SourceOp = "Instance_Transform3",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -38.715, 170.284 } },
				},
				Scale1 = Scale {
					CtrlWShown = false,
					Inputs = {
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Input = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 101.565, 170.284 } },
				},
				MASK_In = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						MultiplyByMask = Input { Value = 1, },
						Alpha = Input { Value = 1, },
						ClipBlack = Input { Value = 1, },
						Input = Input {
							SourceOp = "LinearCloner1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 232, 207.951 } },
				},
				LinearCloner1 = Fuse.LinearCloner {
					CtrlWShown = false,
					Inputs = {
						Width = Input {
							Value = 1920,
							Expression = "Merge4.Background.OriginalWidth",
						},
						Height = Input {
							Value = 1080,
							Expression = "Merge4.Background.OriginalHeight",
						},
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						ClonesCount = Input { Value = 25, },
						Point1 = Input {
							Value = { 0.238541666666667, 0.648148148148148 },
							Expression = "IRIS:GetSourceTool(\"Controls\").PointIn1",
						},
						Point2 = Input {
							Value = { 0.761458333333333, 0.351851851851852 },
							Expression = "IRIS:GetSourceTool(\"Controls\").PointIn4",
						},
						XSize = Input { Value = 1.05, },
						AlphaGain = Input { Value = 0, },
						RandomSeed = Input {
							Value = 26024,
							Expression = "Merge4.Randomize",
						},
						JitterXSize = Input { Value = 0.8, },
						JitterGainNest = Input { Value = 1, },
						Clone = Input {
							SourceOp = "Scale1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 232, 170.284 } },
				},
				Merge4 = Merge {
					CtrlWShown = false,
					Inputs = {
						Foreground = Input {
							SourceOp = "DirectionalBlur1",
							Source = "Output",
						},
						Gain = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 232, 310.271 } },
					UserControls = ordered() {
						Randomize = {
							INP_Integer = true,
							INP_Default = 26024,
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 32767,
							LINKS_Name = "Randomize",
						}
					}
				},
				DirectionalBlur1 = DirectionalBlur {
					CtrlWShown = false,
					Inputs = {
						Blend = Input {
							Value = 0,
							Expression = "math.ceil(Length)",
						},
						Type = Input { Value = 3, },
						Center = Input { Expression = "IRIS:GetSourceTool(\"Controls\").PointIn2", },
						Length = Input { Expression = "math.abs(IRIS:GetSourceTool(\"Controls\").NumberIn3*Diffraction)", },
						Input = Input {
							SourceOp = "MASK_In",
							Source = "Output",
						},
						Diffraction = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 232, 255.03 } },
					UserControls = ordered() {
						Diffraction = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 5,
							INP_Default = 0.5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							ICD_Center = 0.5,
							LINKS_Name = "Diffraction"
						}
					}
				}
			},
		}
	},
	ActiveTool = "ml_LFIris"
}