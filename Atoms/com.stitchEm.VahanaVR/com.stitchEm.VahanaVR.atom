Atom {
	Name = "stitchEm VahanaVR",
	Category = "Bin",
	Author = "stitchEm",
	Version = 2.4,
	Date = {2021, 3, 3},
	Description = [[<h1>VahanaVR by stitchEm</h1>

<p>VahanaVR is a camera-rig independent real-time 360&deg; VR video stitching program.</p>

<p>With VahanaVR you can capture multiple video streams, stitch them instantly into a single 360&deg;x180&deg; video file, preview the results in real-time and stream it to any 360&deg; video player or compatible platform. VahanaVR supports a range of video capture and output hardware, including DeckLink SDI cards from Blackmagic Design, and HDMI capture cards from Magewell, Ximea cameras, and RTMP network broadcasting.</p>

<h2>For More Information</h2>

<p>StitchEm GitHub site:<br>
<a href="https://github.com/stitchEm/StitchEm">https://github.com/stitchEm/StitchEm</a></p>

<p>StitchEm Facebook Group:<br>
<a href="https://www.facebook.com/groups/stitchEm">https://www.facebook.com/groups/stitchEm</a></p>

<h2>License</h2>
<p>VahanaVR is an MIT-licensed open-source project. A license key is not required to run the software. The software was originally developed by VideoStitch SAS. After the company folded in 2018, the source code was acquired by the newly founded non-profit organization stitchEm, to publish it under a free software license.</p>
]],
	Deploy = {

		Windows = {
			"Bin/stitchEm/VahanaVR/LibMWCapture.dll",
			"Bin/stitchEm/VahanaVR/LibXIProperty.dll",
			"Bin/stitchEm/VahanaVR/LibXIStream2.dll",
			"Bin/stitchEm/VahanaVR/Qt5Core.dll",
			"Bin/stitchEm/VahanaVR/Qt5Gui.dll",
			"Bin/stitchEm/VahanaVR/Qt5Multimedia.dll",
			"Bin/stitchEm/VahanaVR/Qt5Network.dll",
			"Bin/stitchEm/VahanaVR/Qt5OpenGL.dll",
			"Bin/stitchEm/VahanaVR/Qt5Widgets.dll",
			"Bin/stitchEm/VahanaVR/VahanaVR.exe",
			"Bin/stitchEm/VahanaVR/audio/qtaudio_windows.dll",
			"Bin/stitchEm/VahanaVR/avcodec-58.dll",
			"Bin/stitchEm/VahanaVR/avformat-58.dll",
			"Bin/stitchEm/VahanaVR/avresample-4.dll",
			"Bin/stitchEm/VahanaVR/avutil-56.dll",
			"Bin/stitchEm/VahanaVR/ceres.dll",
			"Bin/stitchEm/VahanaVR/core_plugins/jpg.dll",
			"Bin/stitchEm/VahanaVR/core_plugins/png.dll",
			"Bin/stitchEm/VahanaVR/core_plugins/tiffPlugin.dll",
			"Bin/stitchEm/VahanaVR/core_plugins_cuda/av_cuda.dll",
			"Bin/stitchEm/VahanaVR/cudart64_100.dll",
			"Bin/stitchEm/VahanaVR/gflags.dll",
			"Bin/stitchEm/VahanaVR/glfw3.dll",
			"Bin/stitchEm/VahanaVR/glog.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qgif.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qico.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qjpeg.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qsvg.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qtga.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qtiff.dll",
			"Bin/stitchEm/VahanaVR/imageformats/qwbmp.dll",
			"Bin/stitchEm/VahanaVR/jpeg62.dll",
			"Bin/stitchEm/VahanaVR/libeay32.dll",
			"Bin/stitchEm/VahanaVR/libmp3lame.dll",
			"Bin/stitchEm/VahanaVR/libpng16.dll",
			"Bin/stitchEm/VahanaVR/librtmp.dll",
			"Bin/stitchEm/VahanaVR/libvideostitch-base.dll",
			"Bin/stitchEm/VahanaVR/libvideostitch-gpudiscovery.dll",
			"Bin/stitchEm/VahanaVR/libvideostitch-gui.dll",
			"Bin/stitchEm/VahanaVR/libvideostitch.dll",
			"Bin/stitchEm/VahanaVR/libvideostitch_cuda.dll",
			"Bin/stitchEm/VahanaVR/libx264-157.dll",
			"Bin/stitchEm/VahanaVR/lzma.dll",
			"Bin/stitchEm/VahanaVR/msvcp140.dll",
			"Bin/stitchEm/VahanaVR/nvml.dll",
			"Bin/stitchEm/VahanaVR/opencv_calib3d.dll",
			"Bin/stitchEm/VahanaVR/opencv_core.dll",
			"Bin/stitchEm/VahanaVR/opencv_features2d.dll",
			"Bin/stitchEm/VahanaVR/opencv_flann.dll",
			"Bin/stitchEm/VahanaVR/opencv_imgproc.dll",
			"Bin/stitchEm/VahanaVR/opencv_video.dll",
			"Bin/stitchEm/VahanaVR/openvr_api.dll",
			"Bin/stitchEm/VahanaVR/platforms/qminimal.dll",
			"Bin/stitchEm/VahanaVR/platforms/qoffscreen.dll",
			"Bin/stitchEm/VahanaVR/platforms/qwindows.dll",
			"Bin/stitchEm/VahanaVR/portaudio_x64.dll",
			"Bin/stitchEm/VahanaVR/ssleay32.dll",
			"Bin/stitchEm/VahanaVR/swresample-3.dll",
			"Bin/stitchEm/VahanaVR/tiff.dll",
			"Bin/stitchEm/VahanaVR/turbojpeg.dll",
			"Bin/stitchEm/VahanaVR/unins000.dat",
			"Bin/stitchEm/VahanaVR/unins000.exe",
			"Bin/stitchEm/VahanaVR/vahana_plugins/decklink.dll",
			"Bin/stitchEm/VahanaVR/vahana_plugins/magewell.dll",
			"Bin/stitchEm/VahanaVR/vahana_plugins/magewellpro.dll",
			"Bin/stitchEm/VahanaVR/vahana_plugins/portaudio.dll",
			"Bin/stitchEm/VahanaVR/vahana_plugins/rtmp.dll",
			"Bin/stitchEm/VahanaVR/vahana_plugins/ximea_64.dll",
			"Bin/stitchEm/VahanaVR/vcruntime140.dll",
			"Bin/stitchEm/VahanaVR/xiapi64.dll",
			"Bin/stitchEm/VahanaVR/zlib1.dll",
			"Docs/VahanaVR/BUILD.md",
			"Docs/VahanaVR/CHANGELOG.md",
			"Docs/VahanaVR/LICENSE-3RD-PARTY-LIBRARIES.md",
			"Docs/VahanaVR/LICENSE-3RD-PARTY-SOURCES.md",
			"Docs/VahanaVR/LICENSE.md",
			"Docs/VahanaVR/README.md",
		},
	},
	InstallScript = {
		Windows = {
			[[if platform == "Windows" then
	-- ----------------------------------------------------------------------
	-- VahanaVR Shortcut
	
	-- Convert the PathMap to an absolute file path
	editorPath = app:MapPath("Reactor:/Deploy/Bin/stitchEm/VahanaVR/VahanaVR.exe")

	-- Add a "VahanaVR.lnk" Shortcut to the Desktop Folder
	CreateShortcut(editorPath, "Desktop:", "VahanaVR for Fusion", "file")
else
	dprintf("[Warning] stitchEm VahanaVR is only available for Windows.\n")
end]],
		},
	},
}
