--[[

Suck Less Loop

-------------------------------------------------------------------
Copyright (c) 2021,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

--]]

require "SuckLessDialogs"

local comp = fusion.CurrentComp

if comp:IsRendering() == true then
	comp:SetData ("Suck Less Loop Render Abort", true)

	SuckLessMsgDialog("Suck Less Loop", "Render Aborting...", "OK")
else
	SuckLessMsgDialog("Suck Less Loop", "Nothing appears to be rendering right now...", "OK")
end