DEBUG = true
print("Euler Filter v1.0")

--[[-- 
	Euler Filter Script for Fusion  
	v1.0, 2021-06-22
	by Bryan Ray for Muse VFX

	=====Overview=====
	
	Animated 3D geometry imported via FBX or Alembic may contain rotations greater than 360 degrees. 
	Fusion "wraps around" these rotations, resulting in the geometry spinning back around to 0 degrees.
	In the 3d viewport, such a discontinuity is undetectable, but if the renderer has motion blur
	enabled, the large sudden rotation will create an artifact.

	To counteract the problem, this script detects large differences between rotation keyframes and 
	modifies the keys to remove the winding errors.


--]]--

-- ===========================================================================
-- globals
-- ===========================================================================
_fusion = nil
_comp = nil
ui = nil 	-- Signposting this global with _ will just be obnoxious, so leave it.
_disp = nil



--==============================================================
-- Main()
--
-- Typical set-up for a script developed in an IDE. Sets globals
-- and connects to the active comp. Initializes UI Manger.
--==============================================================

function main()
	-- get fusion instance
	_fusion = getFusion()

	-- ensure a fusion instance was retrieved
	if not _fusion then
		error("Please open the Fusion GUI before running this tool.")
	end

	-- Set up aliases to the UI Manager framework
	ui = _fusion.UIManager
	_disp = bmd.UIDispatcher(ui)
	
	-- get composition
	_comp = _fusion.CurrentComp
	SetActiveComp(_comp)

	-- ensure a composition is active
	if not _comp then
		error("Please open a composition before running this tool.")
	end

	dprint("\nActive comp is ".._comp:GetAttrs().COMPS_Name)


	-- Ensure a tool is active
	if not tool and _comp.ActiveTool then 
		tool = _comp.ActiveTool 
	elseif not tool then
		local error = _comp:AskUser("ERROR: No tool selected", {})
		return		
	end

	-- Check to be sure we're dealing with a tool that has 3D rotation controls
	if tool and tool.Transform3DOp and tool.Transform3DOp.Rotate then
		-- do nothing
	else
		local error = _comp:AskUser("ERROR: Tool does not have 3D Rotation controls.", {})
		print("ERROR: Tool does not have 3D Rotation controls. Terminating script.")
		return
	end

	-- If the difference between keyframe values is greater than the threshold,
	-- the key will be adjusted.
	local threshold = _comp:AskUser("Threshold", {{ "Angle Threshold:", "Screw", Default = 300.0, Min = 0, Max = 360 }})["Angle Threshold:"]

	dump(threshold)

	-- Assign the control group to a variable for convenience.
	local rotations = {}
	rotations.x = tool.Transform3DOp.Rotate.X
	rotations.y = tool.Transform3DOp.Rotate.Y
	rotations.z = tool.Transform3DOp.Rotate.Z

	-- For each input in rotations, filter keyframes
	for axis, inp in pairs(rotations) do
		local splineout, spline, splinedata
		splineout = inp:GetConnectedOutput()
		if splineout then 
			spline = splineout:GetTool()
			splinedata = spline:GetKeyFrames()
			local previousKey = 'a'
			for i, key in ipairs_sparse(splinedata) do
				if type(previousKey) == 'number' then
					if key[1] - previousKey > threshold then
						print('adjusting frame '..i)
						inp[i] = key[1] - 360
					elseif previousKey - key[1] > threshold then
						print('adjusting frame '..i)
						inp[i] = key[1] + 360
					end
					previousKey = inp[i]
				else 
					previousKey = key[1]
				end
			end
		end
	end

	-- close events
	-- function window.On.main.Close(ev) 
	-- 	_disp:ExitLoop() 
	-- 	status = "Window closed by user."
	-- end

end



-- This function iterates over a sparse ordered table. Necessary, as our keyframe
-- table probably doesn't start at index 1.

function ipairs_sparse(t)
  -- tmpIndex will hold sorted indices, otherwise
  -- this iterator would be no different from pairs iterator
  local tmpIndex = {}
  local index, _ = next(t)
  while index do
    tmpIndex[#tmpIndex+1] = index
    index, _ = next(t, index)
  end
  -- sort table indices
  table.sort(tmpIndex)
  local j = 1

  return function()
    -- get index value
    local i = tmpIndex[j]
    j = j + 1
    if i then
      return i, t[i]
    end
  end
end

--======================== ENVIRONMENT SETUP ============================--

------------------------------------------------------------------------
-- getFusion()
--
-- check if global fusion is set, meaning this script is being
-- executed from within fusion
--
-- Arguments: None
-- Returns: handle to the Fusion instance
------------------------------------------------------------------------
function getFusion()
	if fusion == nil then 
		-- remotely get the fusion ui instance
		fusion = bmd.scriptapp("Fusion", "localhost")
	end
	return fusion
end -- end of getFusion()


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG then _comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()

status = main()