-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromNet"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads a Text string from a network URL.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InURL = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 1
    })

    InViewURL = self:AddInput('View URL', 'View URL', {
        LINKID_DataType = 'Number',
        INPID_InputControl = 'ButtonControl',
        INP_DoNotifyChanged = true,
        INP_External = false,
        ICD_Width = 1,
        INP_Passive = true,
        IC_Visible = true,
        BTNCS_Execute = [[
-- check if a tool is selected
local selectedNode = tool or comp.ActiveTool
if selectedNode then
    local url = selectedNode:GetInput('Input')
    if url then
        bmd.OpenURL('Open', url)
    else
        print('[View URL] URL is nil. Possibly the text is sourced from an external text input connection.')
    end
else
    print('[View URL] Please select the node.')
end
        ]],
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end



function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InURL:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local url = InURL:GetValue(req).Value

    local txt_str = textutils.read_url(url)
    local out = Text(txt_str)

    OutText:Set(req, out)
end
