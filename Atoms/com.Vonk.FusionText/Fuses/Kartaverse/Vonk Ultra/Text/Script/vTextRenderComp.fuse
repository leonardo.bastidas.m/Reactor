-- ============================================================================
-- Fusion Render Node Syntax
-- ============================================================================

-- FusionRenderNode [<filename.comp> | <filename.dfq>] [-quiet] [-render [-frames <frameset>] [-start <frame>] [-end <frame>] [-step <step>] [-quit]] [-listen] [-join <host>] [-log <filename>] [-cleanlog] [-verbose] [-quietlicense] [-version] [-pri high|above|normal|below|idle] [-args [...]]

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextRenderComp"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Launch a command-line Fusion Render Node based .comp or .dfq process via popen.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InFile = self:AddInput("Comp File", "File", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 1
    })

    -- Add a ComboControl control to list the file type that are supported for downloads.
    InRenderMode = self:AddInput("Render Mode", "RenderMode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ComboControl",
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = "Current Frame"},
        {CCS_AddString = "Comp Frame Range"},
        {CCS_AddString = "Custom Frame Range"},
        CC_LabelPosition = "Vertical",
        INP_DoNotifyChanged = true
    })

    InStartFrame = self:AddInput("Start Frame", "StartFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1000,
        INP_MaxScale = 1000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = false,
        LINK_Main = 2,
    })

    InEndFrame = self:AddInput("End Frame", "EndFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1000,
        INP_MaxScale = 1000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = false,
        LINK_Main = 3,
    })

    InStep = self:AddInput("Step", "Step", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinAllowed = 1,
        INP_MinScale = 1,
        INP_MaxScale = 1000,
        INP_Integer = true,
        INP_Default = 1,
        IC_Steps = 1.0,
        IC_Visible = false,
        LINK_Main = 4,
    })

    InVersion = self:AddInput('Render Node Version', 'Version', {
        LINKID_DataType = 'Number',
        INPID_InputControl = 'ComboControl',
        INP_MinScale = 0,
        INP_MaxScale = 3,
        INP_MinAllowed = 0,
        INP_MaxAllowed = 3,
        INP_Default = 3,
        INP_Integer = true,
        ICD_Width = 1.0,
        CC_LabelPosition = 'Horizontal',
        INP_Passive = true,
        LINK_ForceSave = true,
        {CCS_AddString = '9'},
        {CCS_AddString = '16'},
        {CCS_AddString = '17'},
        {CCS_AddString = '18'},
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

--    InInteractive = self:AddInput("Interactive ", "Interactive ", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "CheckboxControl",
--        INP_Integer = true,
--        INP_Default = 1.0,
--        INP_External = false,
--        INP_DoNotifyChanged = true
--    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InStartFrame:SetAttrs({LINK_Visible = visible})
        InEndFrame:SetAttrs({LINK_Visible = visible})
        InStep:SetAttrs({LINK_Visible = visible})
    elseif inp == InRenderMode then
        local visible
        if param.Value == 2.0 then visible = true else visible = false end

        InStartFrame:SetAttrs({IC_Visible = visible})
        InEndFrame:SetAttrs({IC_Visible = visible})
        InStep:SetAttrs({IC_Visible = visible})
    end
end

function System(commandString)
    local handler = io.popen(commandString);
    local response = tostring(handler:read('*a'))
    handler:close()

    -- Trim off the last character which is a newline
    return response:sub(1,-2)
end

function GetPlatform()
    -- Find out the current Fusion host platform (Windows/Mac/Linux)
    local platform = ""

--    if string.find(self.Comp:MapPath("Fusion:/"), "Program Files", 1) then
--        platform = "Windows"
--    elseif string.find(self.Comp:MapPath("Fusion:/"), "PROGRA~1", 1) then
--        platform = "Windows"
--    elseif string.find(self.Comp:MapPath("Fusion:/"), "Applications", 1) then
--        platform = "Mac"
--    else
--        platform = "Linux"
--    end

    if jit.os == "Windows" then
        platform = "Windows"
    elseif jit.os == "Linux" then
        platform = "Linux"
    elseif jit.os == "OSX" then
        platform = "Mac"
    else
        platform = "Linux"
    end

    return platform
end

function Process(req)
    -- [[ Creates the output. ]]
    local file = InFile:GetValue(req).Value
    --local interactive = InInteractive:GetValue(req).Value

    local render_mode = InRenderMode:GetValue(req).Value

    local current_frame = tostring(req.Time)
    local start_frame = tostring(InStartFrame:GetValue(req).Value)
    local end_frame = tostring(InEndFrame:GetValue(req).Value)
    local step = tostring(InStep:GetValue(req).Value)

    local ver = tonumber(InVersion:GetValue(req).Value)
    local fuVersion

    if ver == 0 then
        -- 9
        fuVersion = 9
    elseif ver == 1 then
        -- 16
        fuVersion = 16
    elseif ver == 2 then
        -- 17
        fuVersion = 17
    elseif ver == 3 then
        -- 18
        fuVersion = 18
    else
        -- fallback
        fuVersion = 18
    end
    
    -- print("[Render Node Version]", ver, fuVersion)

    local out = ""

    -- Unchecking the interactive checkbox lets you limit the ProcessOpen function to only occur during a batch render
    -- if (not interactive and not req:IsPreCalc()) or (req:IsInteractive() and interactive) then
    --    out = Text(System(text))
    -- end

    local app = ""
    local platform = GetPlatform()
    if platform == "Windows" then
        -- Running on Windows
        -- app = [[C:\Program Files\Blackmagic Design\Fusion Render Node 17\FusionRenderNode.exe]]
        app = [[C:\Program Files\Blackmagic Design\Fusion Render Node ]] .. fuVersion .. [[\FusionRenderNode.exe]]
    elseif platform == 'Mac' then
        -- Running on Mac
        -- app = "/Applications/Blackmagic Fusion 17 Render Node/Fusion Render Node.app/Contents/MacOS/Fusion Render Node"
        app = [[/Applications/Blackmagic Fusion ]] .. fuVersion .. [[ Render Node/Fusion Render Node.app/Contents/MacOS/Fusion Render Node]]
    elseif platform == "Linux" then
        -- Running on Linux
        -- app = "/opt/BlackmagicDesign/FusionRenderNode17/FusionRenderNode"
        app = [[/opt/BlackmagicDesign/FusionRenderNode]] .. fuVersion .. [[/FusionRenderNode]]
    else
        error("[vTextRenderComp] There is an invalid Fusion platform detected")
    end

    local logFile = "Temp:/Fusion_Render_Node.txt"

    local frame_range_str = ""
    if render_mode == 0 then
        -- Current Frame
        frame_range_str = ' -start '  .. current_frame .. ' -end '  .. current_frame
    elseif render_mode == 1 then
        -- Comp Frame Range
        frame_range_str = ""
    elseif render_mode == 2 then
        -- Custom Frame Range
        frame_range_str = ' -start '  .. start_frame .. ' -end '  ..end_frame .. ' -step ' .. step .. ' '
    else
        -- Current Frame
        frame_range_str = ' -start '  .. current_frame .. ' -end '  ..current_frame
    end

    local command = '"' .. self.Comp:MapPath(app) .. '" "' .. self.Comp:MapPath(file) .. '" -render ' .. frame_range_str .. ' -log "' .. self.Comp:MapPath(logFile) .. '" -cleanlog -verbose -status -quit'
        print(command)

    out = System(command)
    dump(out)
    OutText:Set(req, Text(out))
end
