-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextSortLines"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Sort a multi-line block of text.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InRemoveDuplicates = self:AddInput("Remove Duplicates", "RemoveDuplicates", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local str = InText:GetValue(req).Value
    local sort = InSort:GetValue(req).Value
    local remove_dup = InRemoveDuplicates:GetValue(req).Value

    local lines = {}

    -- Sort text per-line
    local currentLine = 0
    for i in string.gmatch(str, "[^\r\n]+") do
        table.insert(lines, i)
    end

    -- Sort the list alphabetically
    if sort == 1.0 then
        table.sort(lines)
    end

    -- De-duplicate list items
    local result = {}
    if remove_dup == 1.0 then
        -- https://stackoverflow.com/questions/20066835/lua-remove-duplicate-elements/20067270#20067270
        local hash = {}
        for _,v in ipairs(lines) do
           if not hash[v] then
               result[#result + 1] = v
               hash[v] = true
           end
        end
    else
        result = lines
    end

    out = table.concat(result, "\n")

    -- print("[Comp Paths]")
    -- dump(out)

    OutText:Set(req, Text(out))
end
