-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextCompReqTime"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the comp's request time.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]
    InPadding = self:AddInput("Padding", "Padding", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 8,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_Integer = true,
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    Output = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InPadding:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]

    -- Note: If you need to iterate across a range of frames for a temporal effect use Req.Time. Comp.CurrentTime will still report the current timeline frame when a time remapping effect is done.
    local num = req.Time

    local padding = InPadding:GetValue(req).Value
    local result = tostring(string.format('%0' .. padding .. 'd', tonumber(num)))
    local out = Text(result)

    Output:Set(req, out)
end
