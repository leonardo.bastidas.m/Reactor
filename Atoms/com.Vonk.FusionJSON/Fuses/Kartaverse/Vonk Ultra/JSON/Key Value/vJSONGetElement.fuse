-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local json = self and require("dkjson") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vJSONGetElement"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\JSON\\Key Value",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Gets the element of a JSON array.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InJSON = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    InIndex = self:AddInput("Index", "Index", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_Integer = true,
        INP_MinScale = 1,
        INP_MaxScale = 63,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InIndex:SetAttrs({LINK_Visible = true})
        else
            InIndex:SetAttrs({LINK_Visible = false})
        end
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local json_string = InJSON:GetValue(req).Value
    local key = InIndex:GetValue(req).Value

    local value = nil
    local value_str = ""

    if key ~= nil then
        local json_table = jsonutils.decode(json_string)
        value = jsonutils.get(json_table, key)

        if type(value) == "table" then
            value_str = jsonutils.encode(value)
        else
            value_str = tostring(value)
        end
    end

    local out = Text(value_str)
    OutText:Set(req, out)
end
