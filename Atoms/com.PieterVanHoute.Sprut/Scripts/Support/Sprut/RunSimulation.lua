--[[

Sprut2 for Fusion

-------------------------------------------------------------------
Copyright (c) 2020,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

Sprut (c) 2012-2014 Theodor Groeneboom - www.euqahuba.com (theo@euqahuba.com)

Sprut2 is distributed under GNU/GPL. It may be used for commercial purposes, but not redistributed or repackaged, in particular not for any fee.

--]]

require "SprutDialogs"

-- fix for Fu16.2 on Mac (thanks Alexey Bogomolov)
if fu.Version == 16.2 and FuPLATFORM_MAC == true then
    jit.off()
end

-- get our SprutSolver
local comp = fusion.CurrentComp
local currentsolver = comp:GetToolList(true)

-- initialise some variables
local loaderlist = {}
local saverlist = {}
local switchlist = {}
local saversettings = {}
local ___SPRUT_INIT_OUT___
local ___SPRUT_LOOP_IN___
local ___SPRUT_LOOP_OUT___
local ___SPRUT_SWITCH_OUT___

-- get the render range that was set before running the script, because we will want to reset this after
local startframe = comp:GetAttrs().COMPN_RenderStart
local endframe = comp:GetAttrs().COMPN_RenderEnd

if #currentsolver == 1 and (currentsolver[1]:GetAttrs().TOOLS_RegID == "GroupOperator" or currentsolver[1]:GetAttrs().TOOLS_RegID == "MacroOperator") then -- improve this check

	local path = currentsolver[1].CacheFolder[fu.TIME_UNDEFINED]
	if path == "" then
		SprutMsgDialog("Sprut Error", "No Cache Folder is defined in SprutSolver.", "OK")
		print ("Sprut exited. No sim for you.")
		return
	end
	
	local name = currentsolver[1].SolveName[fu.TIME_UNDEFINED]
	local rstart = currentsolver[1].Start[fu.TIME_UNDEFINED]
	local rend = currentsolver[1].End[fu.TIME_UNDEFINED]
	local finit = currentsolver[1].FastInit[fu.TIME_UNDEFINED]
	local compress = currentsolver[1].CompressCacheFiles[fu.TIME_UNDEFINED]
	local renderproxy = currentsolver[1].RenderingProxyScale[fu.TIME_UNDEFINED]
	local renderhiq = currentsolver[1].RenderingHiQ[fu.TIME_UNDEFINED]
	if renderhiq == 1 then 
		renderhiq = true 
	elseif renderhiq == 0 then
		renderhiq = false
	end
	local initrstep = 1
	if finit == 1 then
		initrstep = rend-rstart
	end
	
	-- define which inputs and outputs we're going to work with in the currently selected Macro
	
	local s = currentsolver[1]:GetAttrs()
	loaderlist = comp:GetToolList(false, "Loader")
	saverlist = comp:GetToolList(false, "Saver")
	switchlist = comp:GetToolList(false, "Fuse.Switch")
	
	-- look for our ___SPRUT_INIT_OUT___, ___SPRUT_LOOP_IN___, ___SPRUT_LOOP_OUT___, ___SPRUT_SWITCH_OUT___ inside the selected Solver

	for i, tool in ipairs(saverlist) do
		local a = tool:GetAttrs()
		if a.TOOLH_GroupParent ~= nil then
			if string.find( a.TOOLS_Name, "___SPRUT_INIT_OUT___" ) and a.TOOLH_GroupParent:GetAttrs().TOOLS_Name == s.TOOLS_Name then
				--print("INIT output found")
				--print(a.TOOLS_Name)
				___SPRUT_INIT_OUT___ = tool
				break
			end
		end
	end
	
	for i, tool in ipairs(loaderlist) do
		local a = tool:GetAttrs()
		if a.TOOLH_GroupParent ~= nil then
			if string.find( a.TOOLS_Name, "___SPRUT_LOOP_IN___" ) and a.TOOLH_GroupParent:GetAttrs().TOOLS_Name == s.TOOLS_Name then
				--print("LOOP input found")
				--print(a.TOOLS_Name)
				___SPRUT_LOOP_IN___ = tool
				break
			end
		end
	end
	
	for i, tool in ipairs(saverlist) do
		local a = tool:GetAttrs()
		if a.TOOLH_GroupParent ~= nil then
			if string.find( a.TOOLS_Name, "___SPRUT_LOOP_OUT___" ) and a.TOOLH_GroupParent:GetAttrs().TOOLS_Name == s.TOOLS_Name then
				--print("LOOP output found")
				--print(a.TOOLS_Name)
				___SPRUT_LOOP_OUT___ = tool
				break
			end
		end
	end

	for i, tool in ipairs(switchlist) do
		local a = tool:GetAttrs()
		if a.TOOLH_GroupParent ~= nil then
			if string.find( a.TOOLS_Name, "___SPRUT_SWITCH_OUT___" ) and a.TOOLH_GroupParent:GetAttrs().TOOLS_Name == s.TOOLS_Name then
				--print("SWITCH output found")
				--print(a.TOOLS_Name)
				___SPRUT_SWITCH_OUT___ = tool
				break
			end
		end
	end
	
	-- now that we have found the Saver, let's populate it with the Cache Files path set in the Solver, and render it, but only it
	
	rgbapath = path..name.."000000.exr"
	--print(rgbapath)		-- also here better checks are needed
	
	comp:Lock()
	
	-- make sure Sprut can render by setting Render Abort to false in the comp CustomData
	comp:SetData ("Sprut Render Abort", false)
	
	-- get the current state of all Savers and put them in a table, then set all of them to passthrough
	
	for i, tool in ipairs(saverlist) do
		local a = tool:GetAttrs()
			saversettings[tool] = a.TOOLB_PassThrough
			tool:SetAttrs({TOOLB_PassThrough = true})
	end
	
	-- INITIALISE THE SOLVER
	
	if currentsolver[1].SkipInit[fu.TIME_UNDEFINED] == 0 then
	
		-- switch output, enable Saver and set path
		
		___SPRUT_SWITCH_OUT___.Which = 2
		___SPRUT_INIT_OUT___:SetAttrs({TOOLB_PassThrough = false})
		___SPRUT_INIT_OUT___.Clip = rgbapath
		
		-- render, then passthrough our init saver again
		
		print ("Rendering placeholder frames for output...")
		err = comp:Render(true, rstart, rend, initrstep)
		
		___SPRUT_INIT_OUT___:SetAttrs({TOOLB_PassThrough = true})
		
		-- we now have empty frames, hurray, now let's set the ___SPRUT_LOOP_IN___ and ___SPRUT_VECTORS_IN___
		
		___SPRUT_LOOP_IN___.Clip[rstart] = rgbapath
		___SPRUT_LOOP_IN___.GlobalIn[rstart] = rstart
		___SPRUT_LOOP_IN___.ClipTimeStart[rstart] = 0
		
	end
	
	-- RUN THE SIMULATION
	
	-- get and set some preferences
	
	framesatonce = comp:GetPrefs("Comp.Memory.FramesAtOnce")
	comp:SetPrefs("Comp.Memory.FramesAtOnce", 1)
	
	-- now that we have found the Saver, let's turn off all the others and render it
	
	-- switch output to loop out, activate loop out saver
	
	___SPRUT_SWITCH_OUT___.Which = 3
	___SPRUT_LOOP_OUT___:SetAttrs({TOOLB_PassThrough = false})
	___SPRUT_LOOP_OUT___.Clip[rstart] = rgbapath
	
	if compress == 0 then
		___SPRUT_LOOP_OUT___.OpenEXRFormat.Compression = 3
	elseif compress == 1 then
		___SPRUT_LOOP_OUT___.OpenEXRFormat.Compression = 9
	end
	
	--SprutMsgDialog("Sprut", "Sprut is rendering, check the Console for details...", "Abort Render")

	for frame = rstart, rend, 1 do
	
		-- workaround to prevent having to purge cache
		___SPRUT_LOOP_IN___.Loop = 1
		___SPRUT_LOOP_IN___.Loop = 0
		
		--print (comp:GetData ("Sprut Render Abort"))
	
		if comp:GetData ("Sprut Render Abort") == false then
			print ("\n\nRendering frame "..frame.." of "..rend.."\n\n")
			err = composition:Render(true, frame, frame, 1, renderproxy, renderhiq)
		end
		
	end

	-- reset all Savers, switch output and set preferences to initial state
		
	for saver, setting in pairs(saversettings) do
		saver:SetAttrs({TOOLB_PassThrough = setting})
	end
	
	comp:SetAttrs({COMPN_RenderStart = startframe})
	comp:SetAttrs({COMPN_RenderEnd = endframe})
	
	___SPRUT_SWITCH_OUT___.Which = 1
	
	comp:SetPrefs("Comp.Memory.FramesAtOnce", framesatonce)
	comp:SetData ("Sprut Render Abort", false)

	comp:Unlock()

	print ("Sprut is all done for now...")
	
else
	SprutMsgDialog("Sprut Error", "Please make sure one SprutSolver is selected before running the simulation.", "OK")
	print ("Sprut exited. No sim for you.")
	return
end



