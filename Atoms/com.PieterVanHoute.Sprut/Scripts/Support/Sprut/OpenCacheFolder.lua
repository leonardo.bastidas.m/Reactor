--[[

Sprut2 for Fusion

-------------------------------------------------------------------
Copyright (c) 2020,  Pieter Van Houte
<pieter[at]secondman[dot]com>
-------------------------------------------------------------------

Sprut (c) 2012-2014 Theodor Groeneboom - www.euqahuba.com (theo@euqahuba.com)

Sprut2 is distributed under GNU/GPL. It may be used for commercial purposes, but not redistributed or repackaged, in particular not for any fee.

--]]

require "SprutDialogs"

local comp = fusion.CurrentComp
local currentsolver = comp:GetToolList(true)

if #currentsolver == 1 and (currentsolver[1]:GetAttrs().TOOLS_RegID == "GroupOperator" or currentsolver[1]:GetAttrs().TOOLS_RegID == "MacroOperator") then -- improve this check

	local path = currentsolver[1].CacheFolder[fu.TIME_UNDEFINED]
	if path == "" then
		SprutMsgDialog("Sprut Error", "No Cache Folder is defined in SprutSolver.", "OK")
		return
	end
	
	--dump(app:GetPrefs("Global.Paths.Map"))
	
	-- MapPath resolves any existing path map shortcuts to the full path on disk
	path = app:MapPath(path)
	
	if bmd.direxists(path) == true then
		bmd.openfileexternal("Open", path)
		print ("Opening path in external file requester")
	else
		SprutMsgDialog("Sprut Error", "Invalid path specified.", "OK")
	end
	
	bmd.openfileexternal("Open", path)
	
else
	SprutMsgDialog("Sprut Error", "Please make sure one SprutSolver is selected before running the simulation.", "OK")
	return
end