-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberCompReqTime"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the comp's request time.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]

    Output = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function Process(req)
    -- [[ Creates the output. ]]

    -- Note: If you need to iterate across a range of frames for a temporal effect use Req.Time. Comp.CurrentTime will still report the current timeline frame when a time remapping effect is done.
    local out = Number(req.Time)

    Output:Set(req, out)
end
