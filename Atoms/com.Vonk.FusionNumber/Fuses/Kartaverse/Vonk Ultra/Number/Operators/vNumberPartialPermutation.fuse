-- ============================================================================
-- modules
-- ============================================================================
local json = self and require("dkjson") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberPartialPermutation"
DATATYPE = "Number"

-- ============================================================================
-- Permutations, repetition
-- ============================================================================

local bases = {'A', 'C', 'T', 'G'}

local function allstrings(n, t, k, s)
    k = k or 1
    s = s or {}
    if k > n then
        coroutine.yield(table.concat(s))
    else
        for i = 1, #t
        do
            s[k] = t[i]
            allstrings(n, t, k + 1, s)
        end
    end
end

local function kmer(n, t)
    return coroutine.wrap(allstrings), n, t
end

-- for w in kmer(3, bases)
-- do
--     print(w)
-- end

-- ============================================================================
-- Permutations, no repetition
-- ============================================================================

function map(f, a, ...)
    if a then
        return f(a), map(f, ...)
    end
end

function incr(k)
    return function(a)
        return k > a and a or a+1
    end
end

function combs(m, n)
    if m * n == 0 then
        return {{}}
    end
    local ret, old = {}, combs(m-1, n-1)
    for i = 1, n do
        for k, v in ipairs(old) do
            ret[#ret+1] = {i, map(incr(i), unpack(v))}
        end
    end
    return ret
end

-- ============================================================================
-- Combinations, with repetition
-- ============================================================================

function GenerateCombinations(tList, nMaxElements, tOutput, nStartIndex, nChosen, tCurrentCombination)
	if not nStartIndex then
		nStartIndex = 1
	end
	if not nChosen then
		nChosen = 0
	end
	if not tOutput then
		tOutput = {}
	end
	if not tCurrentCombination then
		tCurrentCombination = {}
	end
 
	if nChosen == nMaxElements then
		-- Must copy the table to avoid all elements referring to a single reference
		local tCombination = {}
		for k,v in pairs(tCurrentCombination) do
			tCombination[k] = v
		end
 
		table.insert(tOutput, tCombination)
		return
	end
 
 	local nIndex = 1
	for k,v in pairs(tList) do
		if nIndex >= nStartIndex then
			tCurrentCombination[nChosen + 1] = tList[nIndex]
			GenerateCombinations(tList, nMaxElements, tOutput, nIndex, nChosen + 1, tCurrentCombination)
		end
 
		nIndex = nIndex + 1
	end
 
	return tOutput
end

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Operators",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the sum of two numbers.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InPossibilities = self:AddInput("Possibilities", "Possibilities", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InSelection = self:AddInput("Selection", "Selection", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 2,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InPossibilities:SetAttrs({LINK_Visible = visible})
        InSelection:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local n1 = InPossibilities:GetValue(req).Value
    local n2 = InSelection:GetValue(req).Value

    local test = {"1","2","3","4","5"}
    
    local combinations = combs(n2, n1)
    
    print(#combinations)
    dump(GenerateCombinations(test, 3))
    
    local combinations_str = json.encode(combinations)
    local out = Text(combinations_str)
    
    OutText:Set(req, out)
end
