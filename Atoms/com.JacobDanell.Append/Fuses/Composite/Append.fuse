--[[--
Append up to 64 Inputs.

Portions of this Fuse Copyright (c) 2010, Stefan Ihringer <stefan@bildfehler.de>
The remainder Copyright (c) 2018, Bryan Ray <xa_bryan@sbcglobal.net> and 
MuseVFX <http://www.musevfx.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------

*	The fuse should be able to be faster. Clip with 30fps playback plays back at 20fps.
	*	If a connected nodetree has something that changes the timing like TimeStretch, stuffs breaks when fuse tried to play that clip as the in and outpoint now is -10000000 to 10000000


Changelog:
version 1.1, 2020-03-04:
* Added looping functionality
* You can now also set the start frame to negative

version 1.0, 2018-09-18:
* Initial release

--]]--

local preTime = 0
local reCalcFootage = true
local firstRun = true
local fusion
local comp
local globalstart
local inputs = {}

FuRegisterClass("Append", CT_SourceTool, {
	REGS_Name = "Append",
	REGS_Category = "Composite",
	REGS_OpIconString = "App",
	REGS_OpDescription = "Append inputs after each other",
	
	REGS_Company = "Jacob Danell",
	REGS_URL = "http://www.emberlight.se",
	REG_Version = 100,
	
	REG_Source_GlobalCtrls = true,
	REG_Source_SizeCtrls = true,
	REG_Source_AspectCtrls = true,
	REG_Source_DepthCtrls = true,

	REG_NoMotionBlurCtrls = true,
	REG_OpNoMask = true,
	REG_NoBlendCtrls = true,
	REG_NoObjMatCtrls = true,
	
	REG_TimeVariant = true,

})

function getFusion()
	return Fusion()
end

function Create()

	InWhich = self:AddInput("Which", "Which", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinAllowed = 1,
		INP_MaxAllowed = 64, 	-- This input is leftover from the Switch Fuse upon which parts of this
		INP_MaxScale = 1,		-- Fuse are based. It is necessary for OnConnected() to work properly.
		INP_Integer = true,
		IC_Steps = 1.0,
		IC_Visible = false,
	})

	InLoop = self:AddInput("Loop", "Loop", {
    	LINKID_DataType = "Number",
    	INPID_InputControl = "CheckboxControl",
  	})

	InStart = self:AddInput("Start Frame", "StartFrame", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 0,
		INP_Integer = true,
		INP_MinScale = 0,
		INP_MaxScale = 100,
		--INP_DelayDefault = true,
	})

	self:AddInput("Input 1", "Input1", {
		LINKID_DataType = "Image",
		INPID_InputControl = "ImageControl",
		ICS_ControlPage = "Controls",
		LINK_Main = 1,
		INP_Required = false,
		INP_SendRequest = false, -- don't send a request for this branch before we actually need it.
		INP_DoNotifyChanged = true,
	})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})

end

-- OnConnected gets called whenever a connection is made to the inputs. A new
-- input is created if something has been connected to the highest input.
function OnConnected(inp, old, new)
	local inpNr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "Input (%d+)"))
	local maxNr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))
	if inpNr then
		if inpNr >= maxNr and maxNr < 64 and new ~= nil then
			InWhich:SetAttrs({INP_MaxScale = inpNr, INP_MaxAllowed = inpNr})

			self:AddInput("Blend "..(inpNr).."-"..(inpNr+1), "Blend"..(inpNr).."-"..(inpNr+1), {
				LINKID_DataType = "Number",
				INPID_InputControl = "ScrewControl",
				ICS_ControlPage = "Controls",
				INP_Integer = true,
				INP_Default = 0,
				INP_MinScale = 0,
				INP_MaxScale = 50,
				INP_MinAllowed = 0,
				INP_DoNotifyChanged = true,
			})
			self:AddInput("Input "..(inpNr + 1), "Input"..(inpNr + 1), {
				LINKID_DataType = "Image",
				INPID_InputControl = "ImageControl",
				ICS_ControlPage = "Controls",
				LINK_Main = (inpNr + 1),
				INP_Required = false,
				INP_SendRequest = false,
				INP_DoNotifyChanged = true,
			})
		end
	end
end

function CountInputs()
	local highestInput = 1
	for i = 2, 64 do
		if self:FindInput("Input"..tostring(i)) ~= nil then
			highestInput = i
		end
	end
	return highestInput
end

function OnAddToFlow()
	-- If a comp is reopened, we need to recreate all inputs that might have
	-- been saved. The weird thing is, that FindInput tells us that an input
	-- exists while it's not visible in the GUI. So we just call AddInput
	-- again, which will make the triangle show up in the GUI.
	-- A problem arises if, for example, inputs 1 and 3 are connected, but 2
	-- isn't. Since Input2 won't be saved in the comp we first need to look
	-- for the highest input we need. Afterwards, OnConnected() will be called
	-- for each of the saved inputs. The additional input needed to connect
	-- further images will be created there.
	local highestInput = CountInputs()

	for i = 2, highestInput do
		self:AddInput("Blend "..(i-1).."-"..(i), "Blend"..(i-1).."-"..(i), {
				LINKID_DataType = "Number",
				INPID_InputControl = "ScrewControl",
				ICS_ControlPage = "Controls",
				INP_Integer = true,
				INP_Default = 0,
				INP_MinScale = 0,
				INP_MaxScale = 50,
				INP_MinAllowed = 0,
				INP_DoNotifyChanged = true,
			})
		self:AddInput("Input "..i, "Input"..i, {
			LINKID_DataType = "Image",
			INPID_InputControl = "ImageControl",
			LINK_Main = i,
			INP_Required = false,
			INP_SendRequest = false,
			INP_DoNotifyChanged = true,
			})
	end
	InWhich:SetAttrs({INP_MaxScale = highestInput, INP_MaxAllowed = highestInput})

	reCalcFootage = true

end

function checkTime(name)
	print(name .. ": " .. (os.clock() - preTime))
	preTime = os.clock()
end

function NotifyChanged(inp, param, time)

	reCalcFootage = true

end

function Process(req)
	--preTime = os.clock()
	--checkTime("start")
	local loop = InLoop:GetValue(req).Value

	if firstRun then
		firstRun = false
		--InStart:SetAttrs({INP_MaxScale = highestInput})
	end

	------- Setup the base image
	local realwidth = Width
	local realheight = Height
	
	-- We'll handle proxy ourselves
	Width = Width / Scale
	Height = Height / Scale
	Scale = 1
	
	local imgattrs = {
		IMG_Document = self.Comp,
		{ IMG_Channel = "Red", },
		{ IMG_Channel = "Green", },
		{ IMG_Channel = "Blue", },
		{ IMG_Channel = "Alpha", },
		IMG_Width = Width,
		IMG_Height = Height,
		IMG_XScale = XAspect,
		IMG_YScale = YAspect,
		IMAT_OriginalWidth = realwidth,
		IMAT_OriginalHeight = realheight,
		IMG_Quality = not req:IsQuick(),
		}
	
	if not req:IsStampOnly() then
		imgattrs.IMG_ProxyScale = 1
	end
	
	if SourceDepth ~= 0 then
		imgattrs.IMG_Depth = SourceDepth
	end
	
	local out = Image(imgattrs)

	--checkTime("emptyImage")

	------- Get dynamically generated inputs
	if reCalcFootage then
		reCalcFootage = false

		--print("Recalced!")
		fusion = getFusion()
		comp = fusion.CurrentComp
	end
	local tool = comp:FindTool(self.Name)

	inputs = {}
	local nrInputs = CountInputs() - 1

	if loop == 1 then
		local currentinput = tool:FindMainInput(nrInputs)
		if currentinput:GetConnectedOutput() ~= nil then
			local connectedtool = currentinput:GetConnectedOutput():GetTool()
			local connectedtoolend = math.floor(connectedtool:GetAttrs().TOOLNT_Region_End[1])
			local blendtime = self:FindInput("Blend"..(nrInputs).."-"..(nrInputs+1)):GetValue(req).Value
			local connectedtoolstart = connectedtoolend - blendtime + 1
			inputs[#inputs+1] = { ["input"] = nrInputs, ["start"] = connectedtoolstart, ["end"] = connectedtoolend, ["range"] = connectedtoolend - connectedtoolstart + 1, ["blend"] = blendtime}
		end
	end

	-- go through all the inputs to get the input ID, startframe, end frame and range, and store those in a nested table 
	for i = 1, nrInputs do
		local currentinput = tool:FindMainInput(i)
		if currentinput:GetConnectedOutput() ~= nil then
			local connectedtool = currentinput:GetConnectedOutput():GetTool()
			local connectedtoolstart = math.floor(connectedtool:GetAttrs().TOOLNT_Region_Start[1])
			local blendtime = self:FindInput("Blend"..(i).."-"..(i+1)):GetValue(req).Value
			local connectedtoolend = math.floor(connectedtool:GetAttrs().TOOLNT_Region_End[1])

			if loop == 1 and i == nrInputs then -- If last connection and looping
				connectedtoolend = math.floor(connectedtool:GetAttrs().TOOLNT_Region_End[1]) - blendtime
				blendtime = 0
			end

			inputs[#inputs+1] = { ["input"] = i, ["start"] = connectedtoolstart, ["end"] = connectedtoolend, ["range"] = connectedtoolend - connectedtoolstart + 1, ["blend"] = blendtime}
		end
	end

	--checkTime("getInputs")

	------- Calculate playback
	local currenttime = req.Time
	local startFrame = InStart:GetValue(req).Value
	local fetchinput = 0
	local range = 0
	local fetchframe = 0
	local clipprogress = 0
	local timevalid = false
	local blendtime = false
	local blendamount = 0

	local validrange = 0
	-- calculate the length of the valid range across all inputs
	for i, v in ipairs(inputs) do
		validrange = validrange + inputs[i]["range"]
		-- Remove the blending-time from the valid range
		if i > 1 then
			validrange = validrange - inputs[i - 1]["blend"]
		end
	end
	-- Add the start frame to the range for it to be correct

	if loop == 1 then
		while currenttime >= validrange do
			currenttime = currenttime - validrange
		end
	end

	
	while currenttime - startFrame >= range and currenttime - startFrame < validrange do
		timevalid = true
		fetchinput = fetchinput + 1
		fetchframe = currenttime - startFrame - range + inputs[fetchinput]["start"]
		range = range + inputs[fetchinput]["range"] - inputs[fetchinput]["blend"]
		if fetchinput == nrInputs and loop == 0 then
			range = range + inputs[fetchinput]["blend"]
		end
	end

	--checkTime("calc outputs")

	local img1 = out
	local img2 = out
	if timevalid == true then

		-- Check if there should be some blending between inputs
		if fetchinput >= 2 then

			clipprogress = fetchframe - inputs[fetchinput]["start"]

			if clipprogress < inputs[fetchinput - 1]["blend"] then
				blendtime = true
				-- Add 1 frame to clipprogress and blend time so it all starts at 1
				blendamount = (clipprogress + 1) / (inputs[fetchinput - 1]["blend"] + 1)
			end

		end
	
		local inp = self:FindInput("Input"..inputs[fetchinput]["input"])
		if inp ~= nil then
			img1 = inp:GetSource(fetchframe, req:GetFlags())
			if img1 ~= nil then
				if req:IsPreCalc() then
					local img1 = Image({IMG_Like = img1, IMG_NoData = true})
				end
			else
				img1 = out
			end
		end

		-- Blend the image if needed
		local fetchframe2 = 0
		if blendtime then

			fetchframe2 = clipprogress + inputs[fetchinput - 1]["start"] + inputs[fetchinput - 1]["range"] - inputs[fetchinput - 1]["blend"]

			inp = self:FindInput("Input"..inputs[fetchinput - 1]["input"])
			if inp ~= nil then
				img2 = inp:GetSource(fetchframe2, req:GetFlags())
				if img2 ~= nil then
					if req:IsPreCalc() then
						local img2 = Image({IMG_Like = img2, IMG_NoData = true})
					end
				else
					img2 = out
				end
			end

			img1 = img2:BlendOf(img1, blendamount)
		end
	end

	--checkTime("get outputs")

	OutImage:Set(req, img1)
	
end