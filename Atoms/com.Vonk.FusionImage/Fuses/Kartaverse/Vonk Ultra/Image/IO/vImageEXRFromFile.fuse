-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageEXRFromFile"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads an EXR image from a file.",
    REGS_OpIconString = FUSE_NAME,

    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})


function Create()
    InFilename = self:AddInput('Filename', 'Filename', {
        LINKID_DataType = 'Text',
        INPID_InputControl = "TextEditControl",
        INP_Passive = true,
        LINK_ForceSave = true,
        TEC_Lines = 1,
        LINK_Main = 1,
    })

--  InFilename = self:AddInput('Filename', 'Filename', {
--      LINKID_DataType = 'Text',
--      INPID_InputControl = 'FileControl',
--      FC_IsSaver = true,
--      FC_ClipBrowse = true,
--      FCS_FilterString = 'OpenEXR Files (*.exr)|*.exr|',
--      INP_Passive = true,
--      LINK_ForceSave = true,
--      LINK_Main = 1,
--  })

    InPart = self:AddInput("EXR Part Number", "Part", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 1000,
        INP_MinAllowed = 1,
        INP_Default = 1,
        INP_Integer = true,
        -- LINK_Main = 2,
    })

    OutImage = self:AddOutput('Output', 'Output', {
        LINKID_DataType = 'Image',
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
end

function Process(req)
    local filename = InFilename:GetValue(req).Value
    local part_number = InPart:GetValue(req).Value
    local out

    if bmd.fileexists(filename) then
        local exr = EXRIO()
        -- exr:ReadOpen(self.Comp:MapPath(filename), req.Time)
        exr:ReadOpen(self.Comp:MapPath(filename), -1)

        if exr:ReadHeader() then
            dispw = exr:DisplayWindow(1)
            dataw = exr:DataWindow(1)
            local ox, oy = dispw.left, dispw.bottom
            local w, h = dispw.right - dispw.left, dispw.top - dispw.bottom

            local imgw = ImgRectI(dataw)
            imgw:Offset(-ox, -oy)

            out = Image({
                IMG_Width = w,
                IMG_Height = h,
                IMG_Depth = IMDP_128bitFloat,
                IMG_DataWindow = imgw,
                IMG_NoData = req:IsPreCalc(),
                IMG_YScale = 1.0/exr:PixelAspectRatio(part_number),
            })

            if not req:IsPreCalc() then
                exr:Part(part_number)

                exr:Channel("R", ANY_TYPE, 1, CHAN_RED)
                exr:Channel("G", ANY_TYPE, 1, CHAN_GREEN)
                exr:Channel("B", ANY_TYPE, 1, CHAN_BLUE)
                --exr:Channel("A", ANY_TYPE, 1, CHAN_ALPHA, 1.0)

                exr:ReadPart(part_number, {out})
            end
        end

        exr:Close()

        err = exr:GetLastError()
        if #err > 0 then
            print(bmd.writestring(err))
        end
    else
        error(string.format("[EXRIO] Image '%s' missing from disk at frame %d.", filename, req.Time))
    end

    OutImage:Set(req, out)
end
