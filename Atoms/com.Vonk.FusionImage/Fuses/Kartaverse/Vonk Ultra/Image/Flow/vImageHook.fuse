--[[--
--]]--

-- ============================================================================
-- constants
-- ============================================================================
DATATYPE = "Image"
-- Don't know the proper format

-- REG_VERSION = 
-- Menu structure it'll be displayed in
REGS_CATEGORY = "Kartaverse\\Vonk Ultra\\Image\\Flow"
-- Used to appear in "About" menu
REGS_COMPANY = "Vonk"
-- Used to appear in "About" menu
REGS_HELPTOPIC = "https://gitlab.com/AndrewHazelden/Vonk/"
-- Name displayed in menu
REGS_NAME = "vImageHook"
-- Description displayed in "About" menu
REGS_OPDESCRIPTION = "vImageHook"
-- Name inbetween round brackets
REGS_OPICONSTRING = "vHook"
-- Name as it will appear in the raw comp file
TOOLS_REGID = "vImageHook"
BTN_WIDTH = 0.33333

-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- variables
-- ============================================================================

-- ============================================================================
-- utils
-- ============================================================================

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(TOOLS_REGID, CT_Tool, {
    --REG_Fuse_NoEdit = true,
    --REG_Fuse_NoReload = true,
    --REG_Fuse_TilePic = fuse_pic,
    REG_NoBlendCtrls = true,
    REG_NoCommonCtrls = true,
    REG_NoMotionBlurCtrls = true,
    REG_NoObjMatCtrls = true,
    REG_NoPreCalcProcess = true,  -- call Process for precalc requests (instead of PreCalcProcess)
    REG_OpNoMask = true,
    REG_SupportsDoD = true,   -- this tool supports DoD
    --REG_TimeVariant = true,
    --REG_Unpredictable = true, -- this tool shall never be cached
    REGID_DataType      = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REGS_Category = REGS_CATEGORY,
    REGS_Company = REGS_COMPANY,
    REGS_HelpTopic = REGS_HELPTOPIC,
    REGS_Name = REGS_NAME,
    REGS_OpDescription = REGS_OPDESCRIPTION,
    REGS_OpIconString = REGS_OPICONSTRING,
})

function Create()
    GetColor = [[
tool.TileColor = tool.Input:GetConnectedOutput():GetTool().TileColor
tool.TextColor = tool.Input:GetConnectedOutput():GetTool().TextColor
    ]]

    GetName = [[
NewName = "HOOK_" .. tool.Input:GetConnectedOutput():GetTool():GetAttrs().TOOLS_Name
tool:SetAttrs({TOOLS_Name = NewName})
    ]]

    GetSource = [[
comp:SetActiveTool(tool.Input:GetConnectedOutput():GetTool())
    ]]

    -- Remove the default "Controls" page from the Fuse GUI
    self:RemoveControlPage("Controls")

    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })

    self:AddControlPage("RegEx")

    InRegEx = self:AddInput("Pattern", "RegExString", {
        INPID_InputControl = "TextEditControl",
        INP_External = true,
        INP_Passive = true,
        LINKID_DataType = "Text",
        LINK_Main = 2,
        LINK_Visible = false,
        TEC_Lines = 1,
    })

    InRegExPriority = self:AddInput("Priority", "RegExPriority", {
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_External = true,
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 0,
        INP_MinScale = 0,
        INP_Passive = true,
        LINKID_DataType = "Number",
        LINK_Main = 3,
        LINK_Visible = false,
    })

    self:AddControlPage("Template")

    InTemplateLoader = self:AddInput("Loader", "TemplateLoader", {
        LINKID_DataType = "Image",
        LINK_Main = 4,
        INPID_InputControl = "ImageControl",
        LINK_Visible = false,
    })

    InShowTemplateLoader = self:AddInput("Show Loader", "ShowTemplateLoader", {
        INPID_InputControl = "CheckboxControl",
        INP_Default = 0.0,
        INP_DoNotifyChanged = true,
        INP_External = false,
        INP_Integer = true,
        LINKID_DataType = "Number",
    })

    InTemplateNodeGraph = self:AddInput("Node Graph", "TemplateNodeGraph", {
        INPID_InputControl = "TextEditControl",
        INP_External = true,
        INP_Passive = true,
        LINKID_DataType = "Text",
        LINK_Visible = false,
        TEC_Wrap = true,
    })

    InEnforceTemplateNodeGraph = self:AddInput("Always Apply", "EnforceTemplateNodeGraph", {
        INPID_InputControl = "CheckboxControl",
        INP_Default = 0.0,
        INP_External = false,
        INP_Integer = true,
        LINKID_DataType = "Number",
    })

    self:AddControlPage("Utility")

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        INPID_InputControl = "CheckboxControl",
        INP_Default = 1.0,
        INP_DoNotifyChanged = true,
        INP_External = false,
        INP_Integer = true,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
        LINKID_DataType = "Number",
    })

    GetColorBtn = self:AddInput("Get Color", "GetColorBtn", {
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetColor,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    GetNameBtn = self:AddInput("Get Name", "GetNameBtn", {
        INPID_InputControl = 'ButtonControl',
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetName,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    GetSourceBtn = self:AddInput("Get Source", "GetSourceBtn", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetSource,
        ICD_Width = BTN_WIDTH,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    self:AddControlPage("Pipeline")

    InPipelineProcess = self:AddInput("Process", "Process", {
        INPID_InputControl = "CheckboxControl",
        INP_Default = 1.0,
        INP_External = false,
        INP_Integer = true,
        LINKID_DataType = "Number",
    })

    OutImage = self:AddOutput("Output", "Output",{
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then 
            visible = true
        else
            visible = false
        end

        InImage:SetAttrs({LINK_Visible = visible})
    elseif inp == InShowTemplateLoader then
        local visible
        if param.Value == 1.0 then
            visible = true
        else
            visible = false
        end

        InTemplateLoader:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    local img = InImage:GetValue(req)

    local out = Image({
        IMG_DataWindow = img.DataWindow,
        IMG_Like = img,
    })

    OutImage:Set(req, img)
end
