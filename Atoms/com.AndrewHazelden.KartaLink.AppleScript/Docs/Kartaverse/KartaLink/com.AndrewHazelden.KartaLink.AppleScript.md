# kvrAppleScript fuses

v5.0 2022-05-04 04.17 PM  
By Andrew Hazelden  

## Overview:
Run macOS based Apple Script code snippets from inside a Fusion Studio node-graph. This unlocks node-based automation techniques such as controlling external applications via Apple Events and OSAX (Open Scripting Architecture Extension) scripting.

## Usage:

1. Create a new comp.

2. Add a "kvrAppleScript" image or text based fuse to the comp.

3. In the Inspector window, type your code into the text field labelled "AppleScript Code".

4. The first time the fuse is run, you need to approve a macOS security message that says:

        "Fusion.app" wants access to control "System Events.app". Allowing control will provide access to documents and data in "System Events.app" and to perform actions within that app.

    You need to click the "OK" button to continue.
    
5. If you want to learn more about scriptable programs, open the Apple Script Editor and then select the "File > Open Dictionary..." menu item. You could also use the "Record" button to save a series of interactions into the script editor window. Copy/Paste this code back into the "kvrAppleScript" fuse's interface in the Resolve/Fusion Inspector view.

## Tip:

If you only want Apple Script code run when a Fusion batch render is occurring, uncheck the "Interactive Render" checkbox.

The kvrAppleScript node as has a Text data type based input connection named "Script". This input connection supports the use of external Vonk nodes like "TextCreateMultiline" or "vTextSubFormatMultiline" as the source of the code that will be executed when the node is rendered.

## Apple Script Examples:

        -- Play a beep sound
        beep


        -- Mute the sound output
        set volume with output muted


        -- Open the BMD Support Center page
        tell application "Safari"
            activate
            tell window 1
                set current tab to (make new tab with properties {URL:"https://www.blackmagicdesign.com/support/family/davinci-resolve-and-fusion"})
            end tell
        end tell


        -- Send an Apple Messages based SMS to a contact
        tell application "Messages"
            -- Enter your contact's phone number between the quotes
            set contact to participant "1 (000) 000-0000"
    
            send "Rendering in progress..." to contact
        end tell


        -- Change the desktop pattern
        tell application "System Events"
            tell every desktop
                set picture to "/Library/Application Support/Blackmagic Design/Fusion/Reactor/Deploy/Bin/KartaVR/bonus/KartaVR Desktop Pattern 3440x1440.png"
            end tell
        end tell

