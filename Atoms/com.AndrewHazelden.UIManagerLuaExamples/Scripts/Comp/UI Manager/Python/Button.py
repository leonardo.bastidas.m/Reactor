ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({'WindowTitle': 'My First Window', 'ID': 'MyWin', 'Geometry': [100, 100, 200, 50], 'Spacing': 0,},[
	ui.VGroup({'Spacing': 0,},[
		# Add your GUI elements here:
		ui.HGroup({},[
			# Add foru buttons that have an icon resource attached and no border shading
			ui.Button({
				'ID': 'B',
				'Text': 'The Button Label',
			}),
		]),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:
def _func(ev):
	print('Button Clicked')
	disp.ExitLoop()
dlg.On.B.Clicked = _func

dlg.Show()
disp.RunLoop()
dlg.Hide()
