{
	Tools = ordered() {
		RsVectorFlipper = MacroOperator {
			CtrlWZoom = false,
			NameSet = true,
			Inputs = ordered() {
				Comments = Input { Value = "Redshift's Z axis is flipped, and 3DS Max is both Z-up and *also* flips its Z axis. This macro conforms the vector passes to Fusion's. \n\nby Bryan Ray for Muse VFX\nhttp://www.musevfx.com", },
				Input1 = InstanceInput {
					SourceOp = "vecFlipperIn",
					Source = "Input",
				}
			},
			Outputs = {
				Output1 = InstanceOutput {
					SourceOp = "choose_dcc",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo { Pos = { 275, -313.5 } },
			Tools = ordered() {
				choose_dcc = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input { Expression = "RsVectorFlipper.RenderSource", },
						Background = Input {
							SourceOp = "hou_doRGB",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "max_doRGB",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 247.5, 109.15 } },
				},
				HoudiniVectors_noRGB = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "HoudiniVectors_1LUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "HoudiniVectors_1LUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "HoudiniVectors_1LUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "HoudiniVectors_1LUTIn4",
							Source = "Value",
						},
						AuxChannelNest = Input { Value = 1, },
						YNormalExpression = Input { Value = "nz1", },
						ZNormalExpression = Input { Value = "-ny1", },
						YPositionExpression = Input { Value = "pz1", },
						ZPositionExpression = Input { Value = "-py1", },
						Image1 = Input {
							SourceOp = "vecFlipperIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -82.5, 142.15 } },
				},
				hou_doRGB = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input { Expression = "RsVectorFlipper.AffectRGB", },
						Background = Input {
							SourceOp = "HoudiniVectors_noRGB",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "HoudiniVectors",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 82.5, 142.15 } },
				},
				HoudiniVectors = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "CustomTool3_1LUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "CustomTool3_1LUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "CustomTool3_1LUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "CustomTool3_1LUTIn4",
							Source = "Value",
						},
						BlueExpression = Input { Value = "-b1", },
						AuxChannelNest = Input { Value = 1, },
						YNormalExpression = Input { Value = "nz1", },
						ZNormalExpression = Input { Value = "-ny1", },
						YPositionExpression = Input { Value = "pz1", },
						ZPositionExpression = Input { Value = "-py1", },
						Image1 = Input {
							SourceOp = "vecFlipperIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -82.5, 109.15 } },
				},
				max_doRGB = Dissolve {
					Transitions = {
						[0] = "DFTDissolve"
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Mix = Input { Expression = "RsVectorFlipper.AffectRGB", },
						Background = Input {
							SourceOp = "MaxVectors_noRGB",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "MaxVectors",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 82.5, 43.15 } },
				},
				vecFlipperIn = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Red = Input { Value = 0, },
						Green = Input { Value = 0, },
						Blue = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -247.5, 76.15 } },
				},
				MaxVectors = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "CustomTool2_1LUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "CustomTool2_1LUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "CustomTool2_1LUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "CustomTool2_1LUTIn4",
							Source = "Value",
						},
						GreenExpression = Input { Value = "-b1", },
						BlueExpression = Input { Value = "-g1", },
						AuxChannelNest = Input { Value = 1, },
						YNormalExpression = Input { Value = "-nz1", },
						ZNormalExpression = Input { Value = "-ny1", },
						YPositionExpression = Input { Value = "-pz1", },
						ZPositionExpression = Input { Value = "-py1", },
						Image1 = Input {
							SourceOp = "vecFlipperIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -82.5, 43.15 } },
				},
				MaxVectors_noRGB = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "MaxVectors_1LUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "MaxVectors_1LUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "MaxVectors_1LUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "MaxVectors_1LUTIn4",
							Source = "Value",
						},
						AuxChannelNest = Input { Value = 1, },
						YNormalExpression = Input { Value = "-nz1", },
						ZNormalExpression = Input { Value = "-ny1", },
						YPositionExpression = Input { Value = "-pz1", },
						ZPositionExpression = Input { Value = "-py1", },
						Image1 = Input {
							SourceOp = "vecFlipperIn",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -82.5, 10.15 } },
				}
			},
			UserControls = ordered() {
				RenderSource = {
					{ MBTNC_AddButton = "Houdini/Maya/C4D" },
					{ MBTNC_AddButton = "3DS Max" },
					INP_Integer = true,
					INPID_InputControl = "MultiButtonControl",
					MBTNC_ShowBasicButton = true,
					INP_Default = 0,
					ICS_ControlPage = "Controls",
					LINKID_DataType = "Number",
					MBTNC_ShowName = true,
					MBTNC_StretchToFit = true,
					MBTNC_ShowToolTip = false,
					LINKS_Name = "Render Source"
				},
				AffectRGB = {
					CBC_TriState = false,
					INP_Integer = true,
					LINKID_DataType = "Number",
					ICS_ControlPage = "Controls",
					INP_Default = 0,
					INPID_InputControl = "CheckboxControl",
					LINKS_Name = "Affect RGB"
				}
			}
		},
		HoudiniVectors_1LUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		HoudiniVectors_1LUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		HoudiniVectors_1LUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		HoudiniVectors_1LUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		},
		CustomTool3_1LUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		CustomTool3_1LUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		CustomTool3_1LUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		CustomTool3_1LUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		},
		CustomTool2_1LUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		CustomTool2_1LUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		CustomTool2_1LUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		CustomTool2_1LUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		},
		MaxVectors_1LUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		MaxVectors_1LUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		MaxVectors_1LUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		MaxVectors_1LUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		}
	},
	ActiveTool = "RsVectorFlipper"
}