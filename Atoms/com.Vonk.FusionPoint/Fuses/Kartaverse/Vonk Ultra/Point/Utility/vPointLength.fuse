-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointLength"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "Number",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Measure the distance between two Fusion Point objects.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InPoint1 = self:AddInput("Point1", "Point1", {
        LINKID_DataType = "Point",
        LINK_Main = 1,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.0,
        INP_DefaultY = 0.5,
    })

    InPoint2 = self:AddInput("Point2", "Point2", {
        LINKID_DataType = "Point",
        LINK_Main = 2,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 1.0,
        INP_DefaultY = 0.5,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutNumber = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InPoint1:SetAttrs({LINK_Visible = visible})
        InPoint2:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local point1 = InPoint1:GetValue(req)
    local point2 = InPoint2:GetValue(req)

    local x1 = tonumber(point1.X)
    local y1 = tonumber(point1.Y)

    local x2 = tonumber(point2.X)
    local y2 = tonumber(point2.Y)

    local length = math.sqrt(((x1 - x2)^2) + ((y1 - y2)^2))

    local out = Number(length)
    OutNumber:Set(req, out)
end
