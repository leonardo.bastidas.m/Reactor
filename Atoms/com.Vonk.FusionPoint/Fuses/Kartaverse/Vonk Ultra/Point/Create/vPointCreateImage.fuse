-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointCreateImage"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Fusion Point object with an image visible in the background.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]
    InImage = self:AddInput('Image', 'Image',{
        LINKID_DataType = 'Image',
        INPID_InputControl = 'ImageControl',
        LINK_Main = 1,
    })

    InPoint = self:AddInput("Point", "Point", {
        LINKID_DataType = "Point",
        LINK_Main = 2,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
    })

    InFlipHoriz = self:AddInput("Flip Horiz", "FlipHoriz", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        ICD_Width = 0.5,
        -- INP_DoNotifyChanged = true
    })

    InFlipVert = self:AddInput("Flip Vert", "FlipVert", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        ICD_Width = 0.5,
        -- INP_DoNotifyChanged = true
    })

    InAddMetadata = self:AddInput("Add Metadata", "AddMetadata", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage = self:AddOutput("OutputImage", "OutputImage", {
        LINKID_DataType = 'Image',
        LINK_Visible = true,
        LINK_Main = 1
    })

    OutPoint = self:AddOutput("OutputPoint", "OutputPoint", {
        LINKID_DataType = "Point",
        INPID_InputControl = "OffsetControl",
        LINK_Main = 2
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local point = InPoint:GetValue(req)
    -- local point = InPoint:GetSource(req.Time)

    local img = InImage:GetValue(req)
    local enableMetadata = InAddMetadata:GetValue(req).Value

    local flipH = InFlipHoriz:GetValue(req).Value
    local flipV = InFlipVert:GetValue(req).Value

    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    img:Crop(result, {})

    local meta = result.Metadata or {}

    local outX
    local outY

    if flipH == 1 then
        outX = 1 - point.X
    else
        outX = point.X
    end

    if flipV == 1 then
        outY = 1 - point.Y
    else
        outY = point.Y
    end

    out = Point(outX, outY)

    if enableMetadata == 1 then
        meta.XOffset = outX
        meta.YOffset = outY
        result.Metadata = meta
    end

    OutPoint:Set(req, out)
    OutImage:Set(req, result)
end
