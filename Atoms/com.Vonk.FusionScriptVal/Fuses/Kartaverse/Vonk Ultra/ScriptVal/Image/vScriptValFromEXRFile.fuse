-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValFromEXRFile"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = "Text",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Image",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Convert OpenEXR header, part, and channel data into a ScriptVal.",
    REGS_OpIconString = FUSE_NAME,

    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15

    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("EXR File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "OpenEXR (*.exr)|*.exr|OpenEXR (*.openexr)|*.openexr|Stereo OpenEXR (*.sxr)|Any Filetype (*.*)|*.*|",
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
   if inp == InShowInput then
       local visible
       if param.Value == 1.0 then visible = true else visible = false end
       InFile:SetAttrs({LINK_Visible = visible})
   end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local path, basename = string.match(abs_path, '^(.+[/\\])(.+)')
    if basename then
        name, extension = string.match(basename, '^(.+)(%..+)$')
    end

    local part_number = 1

    local tbl = {}
    tbl.Parts = {}
    tbl.Type = "EXRPart"
    tbl.Source = abs_path
    tbl.Filename = basename
    tbl.Ext = extension

    if bmd.fileexists(abs_path) then
        local exr = EXRIO()
        -- exr:ReadOpen(self.Comp:MapPath(abs_path), req.Time)
        exr:ReadOpen(self.Comp:MapPath(abs_path), -1)
        
        if exr:ReadHeader() then
            -- EXR Parts
            local parts = exr:GetPartNames()
            local total_parts = table.getn(parts)
            -- print("\n[Part List]", total_parts)
            -- dump(parts)
            if total_parts == 0 then
                -- This is a multi-channel image with no parts
                part = 1
                -- EXR Channels
                local channels = exr:GetChannels(part)
                local total_channels = table.getn(channels)
                -- print("\n[Channels List]", total_channels)
                -- dump(channels)

                -- Data Window
                local dispw = exr:DisplayWindow(part)
                local dataw = exr:DataWindow(part)
                local ox, oy = dispw.left, dispw.bottom
                local w, h = dispw.right - dispw.left, dispw.top - dispw.bottom
                local par = exr:PixelAspectRatio(part)

                -- Build the Lua table
                local part_name = tostring(parts[part])

                -- tbl.Parts[part_name] = {}
                tbl.Parts[part] = {}
                tbl.Parts[part].Name = part_name
                tbl.Parts[part].Channels = channels
                tbl.Parts[part].Width = w
                tbl.Parts[part].Height = h
                tbl.Parts[part].CenterX = ox
                tbl.Parts[part].CenterY = oy
                tbl.Parts[part].PixelAspectRatio = par
                -- print("\n[Part " .. tostring(part) .. "]", part_name)
                -- dump(tbl)
            else
                -- This is a multi-part image
                for part = 1, table.getn(parts) do
                    -- EXR Channels
                    local channels = exr:GetChannels(part)
                    local total_channels = table.getn(channels)
                    -- print("\n[Channels List]", total_channels)
                    -- dump(channels)

                    -- Data Window
                    local dispw = exr:DisplayWindow(part)
                    local dataw = exr:DataWindow(part)
                    local ox, oy = dispw.left, dispw.bottom
                    local w, h = dispw.right - dispw.left, dispw.top - dispw.bottom
                    local par = exr:PixelAspectRatio(part)

                    -- Build the Lua table
                    local part_name = tostring(parts[part])

                    -- tbl.Parts[part_name] = {}
                    tbl.Parts[part] = {}
                    tbl.Parts[part].Name = part_name
                    tbl.Parts[part].Channels = channels
                    tbl.Parts[part].Width = w
                    tbl.Parts[part].Height = h
                    tbl.Parts[part].CenterX = ox
                    tbl.Parts[part].CenterY = oy
                    tbl.Parts[part].PixelAspectRatio = par
                    -- print("\n[Part " .. tostring(part) .. "]", part_name)
                    -- dump(tbl)
                end
            end
        end

        exr:Close()

        err = exr:GetLastError()
        if #err > 0 then
            print(bmd.writestring(err))
        end
    else
        error(string.format("[vScriptValFromEXRFile] Image '%s' missing from disk at frame %d.", abs_path, req.Time))
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
