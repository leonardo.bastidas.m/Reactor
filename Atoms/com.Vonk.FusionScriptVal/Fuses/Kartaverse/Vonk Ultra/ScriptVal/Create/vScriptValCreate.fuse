-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValCreate"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = "Text",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Fusion ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Lua Table Text" , "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 25
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt_str = InText:GetValue(req).Value
    local tbl = bmd.readstring(txt_str)

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
