-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValFromListFiles"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a Fusion ScriptVal object from a bmd.readdir() listing of files.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    -- https://www.steakunderwater.com/VFXPedia/96.0.243.189/indexf069.html?title=Eyeon:Script/Reference/Applications/Fuse/Classes/Input/FileControl
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_PathBrowse = true, -- The dialog will only allow you to select folders
        LINK_Main = 1
    })

    InPattern = self:AddInput("Pattern", "Pattern", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        INPS_DefaultText = "*",
        TEC_Lines = 1,
        LINK_Main = 2
    })

--    InMode = self:AddInput("Mode", "Mode", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "MultiButtonControl",
--        MBTNC_ForceButtons = true,
--        --MBTNC_ShowBasicButton = false,
--        MBTNC_ShowName = true,
--        INP_External = false,
--        INP_Default = 0,
--        {MBTNC_AddButton = "List Files" , MBTNCD_ButtonWidth = 0.5},
--        {MBTNC_AddButton = "List Directories" , MBTNCD_ButtonWidth = 0.5},
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
        InPattern:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local path = InText:GetValue(req).Value
    local pattern = InPattern:GetValue(req).Value
    -- local mode = InMode:GetValue(req).Value

    -- Add the platform specific folder slash character
    local osSeparator = package.config:sub(1,1)

    local path_abs = self.Comp:MapPath(path .. osSeparator .. pattern)
--    print(path_abs)

    local tbl = {}
    if path ~= "" then
        tbl = bmd.readdir(path_abs)
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
