-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValGetToNumber"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Key Value",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Gets the value of a ScriptVal key as a Fusion Number.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InKey = self:AddInput("Key", "Key", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutNumber = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InKey:SetAttrs({LINK_Visible = true})
        else
            InKey:SetAttrs({LINK_Visible = false})
        end
    end
end

function get(t, key)
    --[[
        Returns the value of a key in a table.

        :param t: Table to get key value for.
        :type t: table

        :param key: Key to get value of.
        :type key: string

        :rtype: ?
    ]]
    local value = nil
    local found = false

    for k, v in pairs(t) do
        if k == key then
            value = v
            found = true
            break
        end
    end

    if not found then
        error(string.format("no key '%s' found in ScriptVal table", key))
    end

    return value
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local key = InKey:GetValue(req).Value

    local value = nil
    local value_str = ""
    local value_num

    if key ~= nil then
        value = get(tbl, key)

        if type(value) == "table" then
            -- print("[ScriptvalGet] From a table type")
            value_num = tonumber(bmd.writestring(value))
        elseif type(value) == "number" then
            -- print("[ScriptvalGet] From a number type")
            value_num = tonumber(value)
        elseif type(value) == "string" then
            -- print("[ScriptvalGet] From a string type")
            value_num = tonumber(value)
        else
            -- print("[ScriptvalGet] From another type")
            value_num = tonumber(value)
        end
    end

    -- print(value_num)
    local out = Number(value_num)

    OutNumber:Set(req, out)
end
