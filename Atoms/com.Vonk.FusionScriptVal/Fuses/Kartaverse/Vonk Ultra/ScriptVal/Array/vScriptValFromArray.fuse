-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValFromArray"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "ScriptVal",
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Array",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Casts a JSON array into a ScriptVal object.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local array_str = InText:GetValue(req).Value
    local sort = InSort:GetValue(req).Value

    local json_table = jsonutils.decode(array_str)

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(json_table.array)
    end

    local tbl = {}
    if json_table and json_table.array then
        tbl = json_table.array
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
