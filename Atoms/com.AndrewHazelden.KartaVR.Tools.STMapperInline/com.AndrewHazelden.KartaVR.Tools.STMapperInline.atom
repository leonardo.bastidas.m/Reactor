Atom {
	Name = "STMapperInline Effects Template",
	Category = "Resolve",
	Author = "Andrew Hazelden",
	Version = 5,
	Date = {2021, 12, 24},
	Description = [[<p>The STMapperInline macro is a companion macro that works alongside Jacob Dannel's excellent STMapper.fuse (which is also available in Reactor). STMapperInline was implemented as an Effects Template for the Resolve Edit page. Now you can do ST Map/UV Pass warping live on the Resolve Edit page by dropping the macro onto a clip in your timeline.</p>
	
<h2>For More Information</h2>

<p>There is a markdown formatted guide "com.AndrewHazelden.KartaVR.Tools.STMapperInline.md" for STMapperInline that can be found on disk at:<br>

<a href="file://Reactor:/Deploy/Docs/">Reactor:/Deploy/Docs/</a></p>

<p>To learn more about Jacob Dannel's original STMapper fuse:<br>
<a href="https://www.steakunderwater.com/wesuckless/viewtopic.php?f=33&t=4429">https://www.steakunderwater.com/wesuckless/viewtopic.php?f=33&t=4429</a></p>

]],
	Deploy = {
		"Comps/KartaVR/STMapperInline/Media/TinyPlanet_ST.0000.exr",
		"Comps/KartaVR/STMapperInline/Media/Under_the_Bridge.mp4",
		"Comps/KartaVR/STMapperInline/STMapperInline_Under_The_Bridge_Tiny_Planet.comp",
		"Docs/com.AndrewHazelden.KartaVR.Tools.STMapperInline.md",
		"Templates/Edit/Effects/KartaVR/Stitching/STMapperInline.setting",
		"Templates/Fusion/KartaVR/Stitching/STMapperInline.setting",
	},
	Dependencies = {
			"com.JacobDanell.STMapper",
	},
}
